/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Face header file
 * 		Face class that contains a list of vertices
*/

#ifndef __FACE_H__WALKUP__
#define __FACE_H__WALKUP__

#include "Vector.h"
#include "VertexList.h"
#include <string>
#include <vector>//for index list
#include <sstream>//for to_obj

class Face {
	public:
		//Face();
		Face(VertexList&, std::string&);
		bool add_vertex(unsigned);
		void check_face();
		std::vector<Face> make_triangular();
		std::string to_obj();
		std::vector<unsigned>& get_vertices();
		std::string& get_material(){ return material; }
		void compute_normal();
		Vector& get_normal(){ return normal; }
		double* a(){ return (*v_list)[index_list[0]]; }
		double* b(){ return (*v_list)[index_list[1]]; }
		double* c(){ return (*v_list)[index_list[2]]; }

	private:
		/*
  		unsigned index1;
		unsigned index2;
		unsigned index3;
		unsigned size;
		*/
		std::vector<unsigned> index_list;
		VertexList *v_list;
		Vector normal;
		std::string material;
};

#endif /* __FACE_H__WALKUP__ */
