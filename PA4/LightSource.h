/*
 * Austin Walkup
 * Oct 23, 2011
 *
 * Light Source header file
 * 		Class that holds the properties for a light source
*/

#ifndef __LIGHT_SOURCE_H__WALKUP__
#define __LIGHT_SOURCE_H__WALKUP__

#include "Vector.h"

typedef struct RGB{
	RGB(): r(0), g(0), b(0) {}
	RGB(unsigned char r, unsigned char g, unsigned char b): r(r), g(g), b(b) {}
	unsigned char r;
	unsigned char g;
	unsigned char b;
} RGB;


class LightSource {
	public:
		LightSource();
		LightSource(double, double, double, double, double, double, double);
		
		RGB get_color() { return light_color; }
		Vector& source() { return light_source; }
	private:
		Vector light_source;
		RGB light_color;
};

#endif /* __LIGHT_SOURCE_H__WALKUP__ */
