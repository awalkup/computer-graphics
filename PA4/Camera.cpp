/*
 * Austin Walkup
 * Oct 10, 2011
 *
 * Camera Class file
 * 		The representation of a camera (holds focal point, VPN, VUP, etc.)
*/

#include "Camera.h"
using std::vector;
using std::string;
using std::ofstream;
using std::stringstream;

/*
 * Default Constructor
*/
Camera::Camera(): name(""), focalL(0){
	focalP = Vector();
	proj = Matrix();
}

/*
 * Constructor
*/
Camera::Camera(string cname, double d, double fpx, double fpy, double fpz,
							double vpnx, double vpny, double vpnz,
							double vupx, double vupy, double vupz): name(cname), focalL((d < 0) ? -d : d) {
	//focalL = (d < 0) ? -d : d;
	focalP = Vector(fpx, fpy, fpz);
	create_proj(vpnx, vpny, vpnz, vupx, vupy, vupz);
}

/*
 * sets the image plane for the camera
*/
void Camera::imgPlane(double minx, double miny, double maxx, double maxy){
	this->minx = minx;
	this->miny = miny;
	this->maxx = maxx;
	this->maxy = maxy;
}

/*
 * create an images file named 'filename' of type 'filetype' (defualt = ppm)
 * Acceptable types {ppm}
 * @param filename - Name of the file to create
 * @param img - A RGB struct array that contains the RGB values for each pixel
 * @param filetype - Extension for the file (jpg, png, etc.)
*/
void Camera::create_image(string filename, vector<vector<RGB> >& img, Filetype filetype){
	stringstream ss;
	ss << filename;
	switch(filetype){
		case PPM: 
			if(filename.rfind(".ppm") == string::npos)
				ss << ".ppm";
			ofstream out(ss.str().c_str());
			/* Standard header:
 			 * P3 - Tells reader this is ASCII color image
 			 * sizex - width of image
 			 * sizey - height of image
 			 * 256 - 8 bit pixel values
 			*/
			out << "P3 " << sizex() << " " << sizey() <<" 256\n";
			for(unsigned i=0; i<img.size(); ++i){
				for(unsigned j=0; j < img[i].size(); ++j){
					out << (int) img[i][j].r << " " 
						<< (int) img[i][j].g << " " 
						<< (int) img[i][j].b << '\n';
				}
			}
			break;
		// Other cases (i.e. JPEG, PNG, etc.)
	}//switch
}

/*
 * Computes the projection matrix for the camera
 * @param vpnx - X-coordinate of VPN
 * @param vpny - Y-coordinate of VPN
 * @param vpnz - Z-coordinate of VPN
 * @param vupx - X-coordinate of VUP
 * @param vupy - Y-coordinate of VUP
 * @param vupz - Z-coordinate of VUP
*/
void Camera::create_proj(double vpnx,double vpny, double vpnz,
				 double vupx, double vupy, double vupz){
	n = Vector(vpnx, vpny, vpnz);
	n.normalize(); // VPN / |VPN|
	Vector vup = Vector(vupx, vupy, vupz);
	u = vup.cross(n); // VUPxn
	u.normalize(); // VUPxn / |VUPxn|
	v = n.cross(u); // nxu

	// Sanity check
	if( n.size() != 4 || u.size() != 4 || v.size() != 4){
		stringstream e;
		e << "Error: Unable to create Camera ";
		e << name << ".\n";
		throw e.str();
	}
	if(abs(n*u) >= 0.1 || abs(n*v) >= 0.1 || abs(u*v) >= 0.1){
		stringstream e;
		e << "Error: Camera vectors are not orthonormal.\n";
		e << "n*u = " << n*u << "\nn*v = " << n*v << "\nu*v = " << u*v <<'\n';
		throw e.str();
	}
	Matrix t = create_translate(-focalP[0], -focalP[1], -focalP[2]);

	Matrix r(u[0], u[1], u[2], 0.0,
			 v[0], v[1], v[2], 0.0,
			 n[0], n[1], n[2], 0.0,
			 0.0 , 0.0 , 0.0 , 1.0 );

	Matrix p(1.0, 0.0, 0.0, 0.0,
			 0.0, 1.0, 0.0, 0.0,
			 0.0, 0.0, 1.0, 0.0,
			 0.0, 0.0, -(1/focalL), 0.0);

	proj = p * r * t;
}//create_proj
