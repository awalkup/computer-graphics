/*
 * Austin Walkup
 * Aug 30, 2011
 *
 * CS410_Lexer class file
 * 		A full lexer for CS410 for Fall 2011
*/

#include "Lexer.h"

using std::string;
using std::stringstream;

/**
 * Constructor
 * @param filename - The name of the file to be parsed
*/
Lexer::Lexer(string filename): GenericLexer(filename){
	//object tokens
	GenericLexer::reserve(string("v"), Token::VERTEX);
	GenericLexer::reserve(string("f"), Token::FACE);
	GenericLexer::reserve(string("g"), Token::GROUP);
	GenericLexer::reserve(string("#"), Token::COMMENT);
	GenericLexer::reserve(string("usemtl"), Token::USE_MATERIAL);
	GenericLexer::reserve(string("\n"), Token::NEWLINE);
	GenericLexer::reserve(string("END_OF_FILE"), Token::END_OF_FILE);
}

/**
 * Gets the next Token in the file
 * @ret - The next Token in the file
*/
Token Lexer::getToken(){
	//gets the next word in the file
	string token = next_word();
	stringstream ss;
	ss << token[0];
	string token_id = ss.str();
	//if token exists as an identifier token (ones in the constructor)
	if(lookup(token_id) == Token::COMMENT || lookup(token) != Token::NONE){
		//check if the token is COMMENT (i.e. ignore it)
		if(lookup(token_id) == Token::COMMENT){
			//gets the next word until we see a newline character
			do{
				ss.str("");
				ss.clear();
				token = next_word();
				ss << token[0];
				token_id = ss.str();
			}while(lookup(token_id) != Token::NEWLINE);
			return getToken();//get the next token and return it
		}//if
		else {
			if(lookup(token) == Token::NONE)
				return Token(line, pos - token.size(), lookup(token_id));
			else
				return Token(line, pos - token.size(), lookup(token));
		}
	}//if

	boost::regex INTEGER("(-)?([0-9]+)");//regex for integers
	boost::regex FLOAT("(-)?([0-9]*\\.[0-9]+)(e(-)?[0-9]+)?");//regex for decimals
	boost::regex ID("[a-zA-Z_\\-\\(\\)][0-9a-zA-Z_\\-\\(\\)]*");//regex for non key words

	if(boost::regex_match(token, INTEGER)){
		return Token(line, pos - token.size(), Token::INTEGER, token);
	}
	else if(boost::regex_match(token, FLOAT)){
		return Token(line, pos - token.size(), Token::FLOAT, token);
	}
	else if(boost::regex_match(token, ID)){
		return Token(line, pos - token.size(), Token::ID, token);
	}
	else{
		stringstream e;
		e<<"An error has occured while parsing the file.\n";
		e<<line<<":"<<pos-token.size()<<" Unexpected value "<<token<<".";
		throw e.str();
	}
}//getToken
