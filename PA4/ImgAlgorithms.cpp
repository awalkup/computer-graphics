/*
 * Austin Walkup
 * Oct 11, 2011
 *
 * ImgAlgorithms class file
 * 		All the algorithms for creating an image are here. Call draw
*/

#include "ImgAlgorithms.h"

using std::vector;
using std::string;
using std::map;
using std::multiset;
using std::pair;

/*
 * Constructor
*/
ImgDrawer::ImgDrawer(double xmin, double ymin, double xmax, double ymax):
		 minx(xmin), miny(ymin), maxx(xmax), maxy(ymax),
		 img( round((ymax - ymin))+1, vector<RGB>(round((xmax - xmin))+1)), 
		 z_buf( round((ymax - ymin))+1, vector<double>(round((xmax - xmin))+1, -DBL_MAX) ) {}

/*
 * Constructor
*/
ImgDrawer::ImgDrawer(double xmin, double ymin, double xmax, double ymax,
		 vector<LightSource> light_sources, map<string, Material> material_map): 
		 minx(xmin), miny(ymin), maxx(xmax), maxy(ymax),
		 img( round((ymax - ymin))+1, vector<RGB>(round((xmax - xmin))+1)), 
		 z_buf( round((ymax - ymin))+1, vector<double>(round((xmax - xmin))+1, -DBL_MAX) ),
		 lights(light_sources), materials(material_map) {}

/*
 * Draws all objects from 'groups'
 * @param groups - The list of objects to draw
 * @param v_list - The list of vertices the objects refer to (pass by value so that data isn't lost)
 * @ret - A vector of RGB values, size of image_width * image_height
*/
vector<vector<RGB> > ImgDrawer::draw_wireFrame(Camera& cam, map<string, Group>& groups, VertexList v_list){
	Matrix& proj = cam.get_proj_matrix();
	for(unsigned i=1; i<=v_list.size(); i++){
		proj * v_list[i];
	}

	for(map<string, Group>::iterator it = groups.begin(); it != groups.end(); ++it){
		vector<Face> f_vec = it->second.get_faces();
		for(unsigned k=0; k<f_vec.size(); k++){
			vector<unsigned> fv_vec = f_vec[k].get_vertices();

			int x1 = round(v_list[ fv_vec[ fv_vec.size()-1 ] ][0]);
			int y1 = round(v_list[ fv_vec[ fv_vec.size()-1 ] ][1]);
	
			for(unsigned i=0; i< fv_vec.size(); ++i){
				int x2 = round(v_list[fv_vec[i]][0]);
				int y2 = round(v_list[fv_vec[i]][1]);

				int x = x2;
				int y = y2;

				if( clipping(x1, y1, x, y) )
					bresenhams(x1, y1, x, y);
	
				x1 = x2;
				y1 = y2;
			}//for vertices
		}//for faces
	}//for groups
	return img;
}//draw wire frame

/*
 * Renders a Goraud Shading image of the world
 * @param cam - The camera that is 'taking' the picture
 * @param groups - The groups in the world
 * @param v_list - All the vertices in the world
 * @ret - Vector of vectors of RGB structs (the image in pixels)
*/
vector<vector<RGB> > ImgDrawer::draw_surfaceRender(Camera& cam, map<string, Group>& groups, VertexList v_list){
	Matrix& proj = cam.get_proj_matrix();
	Vector& focalP = cam.get_focalP();
	vector<pair<double, Vector> > avg_normals(v_list.size());
	for(map<string, Group>::iterator it = groups.begin(); it != groups.end(); ++it){
		vector<Face> f_vec = it->second.get_faces();
		for(unsigned k=0; k<f_vec.size(); k++){
			vector<unsigned> fv_vec = f_vec[k].get_vertices();
			f_vec[k].compute_normal();
			for(unsigned i=0; i< fv_vec.size(); ++i){
				if(avg_normals[fv_vec[i]-1].second * f_vec[k].get_normal() < 0 && avg_normals[fv_vec[i]-1].second.magnatude() != 0)
					f_vec[k].get_normal() = f_vec[k].get_normal()*-1;
				avg_normals[fv_vec[i]-1].second += f_vec[k].get_normal();
				avg_normals[fv_vec[i]-1].first++;
			}
		}
	}

	vector<Point> vert_points(v_list.size());
	for(map<string, Group>::iterator it = groups.begin(); it != groups.end(); ++it){
		vector<Face> f_vec = it->second.get_faces();
		for(unsigned k=0; k<f_vec.size(); k++){
			vector<unsigned> fv_vec = f_vec[k].get_vertices();
			for(unsigned i=0; i< fv_vec.size(); ++i){
				int index = fv_vec[i]-1;
				if(vert_points[index].color.r == (unsigned char)-1){
					double* v = v_list[index+1];
					vert_points[index] = Point(round(v[0]), round(v[1]), vertex_color(Vector(v[0], v[1], v[2]), avg_normals[index].second*(1/avg_normals[index].first), materials[f_vec[k].get_material()], focalP), v[2]);
				}
			}
		}
	}


	for(unsigned i=1; i<=v_list.size(); i++){
		proj.multiply_ignore_z(v_list[i]);
	}


	for(map<string, Group>::iterator it = groups.begin(); it != groups.end(); ++it){
		vector<Face> f_vec = it->second.get_faces();
		for(unsigned k=0; k<f_vec.size(); k++){
			vector<unsigned> fv_vec = f_vec[k].get_vertices();

			multiset<Point> poly_set;

			int prev_vertex = fv_vec[ fv_vec.size()-1 ];
			int x1 = round(v_list[ prev_vertex ][0]);
			int y1 = round(v_list[ prev_vertex ][1]);

			for(unsigned i=0; i< fv_vec.size(); ++i){
				int curr_vertex = fv_vec[i];
				int x2 = round(v_list[curr_vertex][0]);
				int y2 = round(v_list[curr_vertex][1]);

				int x1_t = x1;
				int y1_t = y1;
				int x2_t = x2;
				int y2_t = y2;

				//if( clipping(x1_t, y1_t, x2_t, y2_t) ){

					scanLine_intersection(poly_set, x1_t, y1_t, x2_t, y2_t, vert_points[prev_vertex-1], vert_points[curr_vertex-1]);
				//}
	
				prev_vertex = curr_vertex;
				x1 = x2;
				y1 = y2;
			}//for vertices
			polygon_filling(poly_set);
		}//for faces
	}//for groups
	return img;
}//draw_surfaceRender

/*
 * Renders a raytraced image of the world
 * @param cam - The camera that is 'taking' the picture
 * @param groups - The groups in the world
 * @param v_list - All the vertices in the world
 * @ret - Vector of vectors of RGB structs (the image in pixels)
*/
#include<iostream>
using namespace std;
vector<vector<RGB> > ImgDrawer::draw_raytrace(Camera& cam, map<string, Group>& groups){
std::cout<<"img size: "<<img[0].size()<<"x"<<img.size()<<std::endl;
	Vector& focalP = cam.get_focalP();
//cout<<"1"<<std::endl;
	//#pragma omp parallel for
	//for(int x = minx+1; x < (int)maxx; x++){
	for(unsigned i = 0; i < img.size(); i++){
	//	for(int y = miny+1; y < (int)maxy; y++){
		for(unsigned j = 0; j < img[0].size(); j++){
	//		int th_id = omp_get_thread_num();
			double x = (j+minx);
//cout<<x<<endl;
			double y = -(i+miny);
//cout<<y<<endl;
//cout<<"("<<j<<", "<<i<<") -> ("<<x<<", "<<y<<")"<<endl;
//cout<<"focalP: "<<focalP<<"\nN: "<<cam.get_n()*cam.get_focalLength()<<"\nU: "<<cam.get_u()<<"\nV: "<<cam.get_v()<<endl;
			Ray view(focalP, focalP + cam.get_n()*-cam.get_focalLength() + cam.get_u()*x + cam.get_v()*y);
//cout<<"2"<<std::endl;
//cout<<"Orignal "<<focalP<<" + t"<<cam.get_n()*-cam.get_focalLength() + cam.get_u()*x + cam.get_v()*y<<endl;
			RGB color = ray_reflectance(view, groups, 0);

//cout<<"3"<<std::endl;
			//#pragma omp critical
			{
//cout<<"Thread: "<<th_id<<" has index "<<x<<" "<<y<<endl;
				//set_pixel(x, y, color);
				set_pixel(i, j, color, false);
			}
//cout<<"4"<<std::endl;
		}
	}
	return img;
}//draw_raytracer

/*
 * Sets the pixel specified at the point (x,y)
 * @param x - x-coordinate of the pixel to set
 * @param y - y-coordinate of the pixel to set
*/
inline
void ImgDrawer::set_pixel(int x, int y, RGB color, bool convert) {
//std::cout<<"mapping ("<<x<<", "<<y<<") to ("<<-(y+miny)<<", "<<x-minx<<")"<<std::endl;
	if(convert)
		img[-(y+miny)][x-minx] = color;
	else
		img[x][y] = color;
}

/*
 * Checks if the pixel (x,y) can be set.
 * @param x - x-coordinate of the pixel in the image
 * @param y - y-coordinate of the pixel in the image
 * @ret - True if the pixel can be set, false otherwise
*/
inline
bool ImgDrawer::check_pixel(int x, int y, double depth){
	int t =-(y+miny);
	y = x-minx;
	x = t;
	if((unsigned)x >= z_buf.size() || x < 0 || (unsigned)y >= z_buf[x].size() || y < 0 || z_buf[x][y] > depth)
		return false;
	
	z_buf[x][y] = depth;
	return true;
}

/*
 * Code inspired by http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
 * Preforms Bresenham's to find what points to draw
 * @param x1 - x-coordinate of end-point 1
 * @param y1 - y-coordinate of end-point 1
 * @param x2 - x-coordinate of end-point 2
 * @param y2 - y-coordinate of end-point 2
*/
void ImgDrawer::bresenhams(int x1, int y1, int x2, int y2){
	int dx = (x2 - x1);
	dx = (dx > 0) ? dx : -dx;
	int dy = (y2 -y1);
	dy = (dy > 0) ? dy : -dy;

	int sx = (x1 < x2) ? 1 : -1;
	int sy = (y1 < y2) ? 1 : -1;

	int err = dx - dy;

	do {
		set_pixel(x1, y1);

		int e2 = 2*err;
		if(e2 > -dy){
			err -= dy;
			x1 += sx;
		}
		if(e2 < dx){
			err += dx;
			y1 += sy;
		}
	} while(x1 != x2 || y1 != y2);
}

/*
 * Code inspired by http://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
 * Cohen-Sutherland clipping algorithm, clips the line to be drawn to the field of view
 * modifies the inputs to be the clipped version, 
 * returns true if the line is to be drawn
 * @param x1 - x-coordinate of end-point 1
 * @param y1 - y-coordinate of end-point 1
 * @param x2 - x-coordinate of end-point 2
 * @param y2 - y-coordinate of end-point 2
*/
bool ImgDrawer::clipping(int& x1, int& y1, int& x2, int& y2){
	int p1_code = clipping_helper(x1,y1);
	int p2_code = clipping_helper(x2,y2);

	bool accept = false;
	for(;;){
		if( !(p1_code | p2_code) ){ // Bitwise OR is zero, accept
			accept = true;
			break;
		}
		else if( p1_code & p2_code ) // Bitwise AND is non_zero, reject
			break;
		else {
			int x, y;
			
			//one of the points out of bounds
			int out_code = p1_code ? p1_code : p2_code;

			// Find the intercept
			if(out_code & TOP){
				x = x1 + (x2 - x1) * (maxy - y1) / (y2 - y1);
				y = maxy;
			}
			else if(out_code & BOTTOM){
				x = x1 + (x2 - x1) * (miny+1 - y1) / (y2 - y1);
				y = miny+1;
			}
			else if(out_code & RIGHT){
				x = maxx-1;
				y = y1 + (y2 - y1) * (maxx-1 - x1) / (x2 - x1);
			}
			else if(out_code & LEFT){
				x = minx;
				y = y1 + (y2 - y1) * (minx - x1) / (x2 - x1);
			}
			else {
				throw "An error has occured while finding clipping intercepts";
			}

			if(out_code == p1_code){
				x1 = x;
				y1 = y;
				p1_code = clipping_helper(x1,y1);
			}
			else if(out_code == p2_code){
				x2 = x;
				y2 = y;
				p2_code = clipping_helper(x2,y2);
			}
			else {
				throw "An error has occured while clipping";
			}
		}//else
	}//for

	/*if(accept){
		bresenhams(-(y1+miny), x1-minx, -(y2+miny), x2-minx);
		//bresenhams(x1-minx, -(y1+miny), x2-minx, -(y2+miny));
	}*/
	return accept;
}//clipping

/*
 * Helper method for the clipping algorithm
 * @param x - x-coordinate of end-point
 * @param y - y-coordinate of end-point
 * @ret - an int that is a bit string for the areas where the endpoint violates
*/
int ImgDrawer::clipping_helper(int x, int y){
	int code = INSIDE;
	
	if(x < minx)
		code |= LEFT;
	else if(x >= maxx)
		code |= RIGHT;

	if(y <= miny)
		code |= BOTTOM;
	else if(y > maxy)
		code |= TOP;

	return code;
}

/*
 * Fills in the appropriate pixels in the image, given a set of
 * intersection points.
 * @param poly_set - Set of intersection points
*/
void ImgDrawer::polygon_filling(multiset<Point>& poly_set) {
	if(poly_set.size() == 0)
		return;
	int ymax = poly_set.rbegin()-> y;

	multiset<Point>::iterator pt = poly_set.begin();
	Point prev_pt = *pt;
	++pt;
	for(; pt != poly_set.end(); ++pt){
		int y = pt->y;
		/*if(prev_pt.y != y){
			prev_pt.y = pt->y;
			continue;
		}*/
		if(y != ymax && prev_pt.x != pt->x && prev_pt.y == pt->y){

			int size = pt->x - prev_pt.x;
			size = (size < 0) ? -size : size;
			double* depth = new double[size+1];
			unsigned char* red = new unsigned char[size+1];
			unsigned char* green = new unsigned char[size+1];
			unsigned char* blue = new unsigned char[size+1];
			interpolate(prev_pt.x, pt->x, prev_pt, *pt, red, green, blue, depth);

			int i = 0;
			for(int x = prev_pt.x; x < pt->x; ++x, ++i){
				if(check_pixel(x, y, depth[i])){
					set_pixel(x, y, RGB(red[i], green[i], blue[i]));
				}
			}//for
			delete[] depth;
			delete[] red;
			delete[] green;
			delete[] blue;
		}//if
		prev_pt = *pt;
	}//for
}//polygon filling

/*
 * Compute the intersections between the line (passed in) and the scan lines
 * of the image. Add these Points to poly_set.
 * @param poly_set - Set of Points to add to
 * @param x1 - x-coordinate of the first end-point of the line
 * @param y1 - y-coordinate of the first end-point of the line
 * @param x2 - x-coordinate of the second end-point of the line
 * @param y2 - y-coordinate of the second end-point of the line
*/
void ImgDrawer::scanLine_intersection(multiset<Point>& poly_set, double x1, double y1, double x2, double y2, Point& start, Point& end){
	if((int)y1 == (int)y2) return;
	double ymin, ymax;
	if(y1 < y2){
		ymin = y1;
		ymax = y2;
	}
	else {
		ymin = y2;
		ymax = y1;
	}

	int size = y2 - y1;
	size = (size < 0) ? -size : size;
	unsigned char* red = new unsigned char[size+1];
	unsigned char* green = new unsigned char[size+1];
	unsigned char* blue = new unsigned char[size+1];
	double* depth = new double[size+1];

	interpolate((int)y1, (int)y2, start, end, red, green, blue, depth);

	int i = 0;
	for(double yd = ymin; yd<=ymax; yd++, i++){
		double x = yd;
		if(y2 - y1 == 0) break;
		//if(x2 - x1 != 0)
			x = ceil(x1 + (x2 - x1) * (yd - y1) / (y2 - y1));
		int y = (int)yd;
		Point p((int)x, y, RGB(red[i], green[i], blue[i]), depth[i]);
		poly_set.insert(p);
	}
	delete[] red;
	delete[] green;
	delete[] blue;
	delete[] depth;
}

/*
 * Calculates the color of a vertex
 * @param normal - The average normal vector of the vertex
 * @param face_material - The material that of the face that the vertex belongs to
 * @param focalP - The focal point of the Camera
 * @param light_sources - A list of all the light sources in the world
 * @ret - An RGB color that is the color of the vertex
*/
RGB ImgDrawer::vertex_color(Vector ver, Vector& normal, Material& face_material, Vector& focalP) {
	Vector sum;
	double color[4];
	normal = normal.normalize()*-1;
	for(unsigned i =0; i<lights.size(); i++){
		RGB c = lights[i].get_color();
		color[0] = c.r;
		color[1] = c.g;
		color[2] = c.b;
		color[3] = 1;

		//lambertion
		face_material.get_LRM() * color;
		//Vector ver(vertex[0], vertex[1], vertex[2]);
		//ver.normalize();
		Vector l = Vector(ver, lights[i].source());
		l.normalize();
		if(normal * l < 0){
			//return RGB(0, 0, 0);
			continue;
		}
		sum = sum + (Vector(color[0], color[1], color[2]) * (normal * l));

		//specular reflectance
		Vector col = Vector( c.r, c.g, c.b) * face_material.get_spec_reflect();
		//col.normalize();
		Vector v = Vector(ver, focalP);
		v.normalize();
		if(normal * v < 0){
			v = v*-1;
		}
		Vector r = 2*(l*normal)*normal - l;
		r.normalize();

		double rv = r*v;
		if(rv < 0){
			//return RGB(0, 0, 0);
			continue;
		}
		rv = pow(rv, face_material.get_phong_reflect());
		sum = sum + (col*rv);
	}

	if(sum[0] > 255)
		sum[0] = 255;
	if(sum[1] > 255)
		sum[1] = 255;
	if(sum[2] > 255)
		sum[2] = 255;

	if(sum[0] < 0 || sum[1] < 0 || sum[2] < 0){
		throw "An error has occured while determining the color for a vertex\n";
	}

	return RGB(sum[0], sum[1], sum[2]);
	//return RGB((unsigned char)vertex[0], (unsigned char)vertex[1], (unsigned char)vertex[2]);
}

/*
 * Code inspired by http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
 * Preforms Bresenham's to find what points to draw
 * @param x1 - x-coordinate of end-point 1
 * @param y1 - y-coordinate of end-point 1
 * @param x2 - x-coordinate of end-point 2
 * @param y2 - y-coordinate of end-point 2
 * @param img - an array where the values are stored
*/
void ImgDrawer::bresenhams(int x1, unsigned char y1, int x2, unsigned char y2, unsigned char* img){
	int dx = (x2 - x1);
	dx = (dx > 0) ? dx : -dx;
	int dy = (y2 -y1);
	dy = (dy > 0) ? dy : -dy;

	int sx = (x1 < x2) ? 1 : -1;
	int sy = (y1 < y2) ? 1 : -1;

	int err = dx - dy;

	do {
		img[x1] = y1;

		int e2 = 2*err;
		if(e2 > -dy){
			err -= dy;
			x1 += sx;
		}
		if(e2 < dx){
			err += dx;
			y1 += sy;
		}
	} while(x1 != x2 || y1 != y2);
}

/*
 * Code inspired by http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
 * Preforms Bresenham's to find what points to draw
 * @param x1 - x-coordinate of end-point 1
 * @param y1 - y-coordinate of end-point 1
 * @param x2 - x-coordinate of end-point 2
 * @param y2 - y-coordinate of end-point 2
 * @param img - an array where to store the values
*/
void ImgDrawer::bresenhams(int x1, double y1, int x2, double y2, double* img){
	int dx = (x2 - x1);
	dx = (dx > 0) ? dx : -dx;
	int dy = (y2 -y1);
	dy = (dy > 0) ? dy : -dy;

	int sx = (x1 < x2) ? 1 : -1;
	int sy = (y1 < y2) ? 1 : -1;

	int err = dx - dy;

	do {
		if(x1 == x2) break;
		img[x1] = y1;

		int e2 = 2*err;
		if(e2 > -dy){
			err -= dy;
			x1 += sx;
		}
		if(e2 < dx){
			err += dx;
			y1 += sy;
		}
	} while(x1 != x2 || y1 != y2);
}

/*
 * Interpolates the red green blue and depth values at two point
 * @param start - The starting point
 * @param end - The ending point
 * @param red - The red value array
 * @param green - The green value array
 * @param blue -The blue value array
 * @param depth - The depth value array
*/
void ImgDrawer::interpolate(int y1, int y2, const Point& start, const Point& end, unsigned char* red, unsigned char* green, unsigned char* blue, double* depth){

	int ymin = (y1 < y2) ? y1 : y2;
	int y1t, y2t;
	if(y1 == ymin){
		if(y1 <= 0){
			y1t = y1 + abs(ymin);
			y2t = y2 + abs(ymin);
		}
		else{
			y1t = y1 - abs(ymin);
			y2t = y2 - abs(ymin);
		}
	}
	else {
		if(y2 <= 0){
			y1t = y1 + abs(ymin);
			y2t = y2 + abs(ymin);
		}
		else{
			y1t = y1 - abs(ymin);
			y2t = y2 - abs(ymin);
		}
	}
	//int y1t = (y1 == ymin) ? (y1 <= 0) ? y1+abs(ymin) : y1 - abs(ymin) : (y2<=0) ? y1+abs(ymin) : y1-abs(ymin);
	//int y2t = (y1 == ymin) ? (y1 <= 0) ? y2+abs(ymin) : y2 - abs(ymin) : (y2<=0) ? y2+abs(ymin) : y2-abs(ymin);

	if(red != NULL){
		bresenhams(y1t, start.color.r, y2t, end.color.r, red);
		red[y1t] = start.color.r;
		red[y2t] = end.color.r;
	}

	if(green != NULL){
		bresenhams(y1t, start.color.g, y2t, end.color.g, green);
		green[y1t] = start.color.g;
		green[y2t] = end.color.g;
	}

	if(blue != NULL){
		bresenhams(y1t, start.color.b, y2t, end.color.b, blue);
		blue[y1t] = start.color.b;
		blue[y2t] = end.color.b;
	}

	if(depth != NULL){
		bresenhams(y1t, start.depth, y2t, end.depth, depth);
		depth[y1t] = start.depth;
		depth[y2t] = end.depth;
	}
}//interpolate

/*
 * Compute the color of a pixel defined by the ray view
 * @param view - The ray that defines what point to color
 * @param groups - The set of objects to intersect with
 * @ret - The color at a specific point
*/
RGB ImgDrawer::ray_reflectance(Ray& view, map<string, Group>& groups, int depth, Face* ignore_face){
	if(depth > DEPTH)
		return RGB(0, 0, 0);
	double s_min = DBL_MAX;
	Face* surf_min = NULL;

	for(map<string, Group>::iterator git = groups.begin(); git != groups.end(); ++git){
		for(vector<Face>::iterator fit = git->second.get_faces().begin(); fit != git->second.get_faces().end(); ++fit){
			if(&(*fit) == ignore_face)
				continue;
			double s = view.intersect(*fit);
			if( s > 0 && s < s_min ){
//cout<<"s "<<s<<endl;
				s_min = s;
				surf_min = &(*fit);
			}
		}//for faces
	}//for group

	if(surf_min == NULL)
		return RGB(0,0,0);
//cout<<"Surface Pt: "<<view[s_min]<<" s: "<<s_min<<endl;
//cout<<"Face: A("<<surf_min->a()[0]<<" "<<surf_min->a()[1]<<" "<<surf_min->a()[2]<<" "<<surf_min->a()[3]<<") B("<<surf_min->b()[0]<<" "<<surf_min->b()[1]<<" "<<surf_min->b()[2]<<" "<<surf_min->b()[3]<<") C("<<surf_min->c()[0]<<" "<<surf_min->c()[1]<<" "<<surf_min->c()[2]<<" "<<surf_min->c()[3]<<endl;
	return point_reflectance(view, view[s_min], surf_min->get_normal(), surf_min, view.get_start(), groups, depth);
}//ray_reflectance

/*
 * Find the color of the point (raytracing)
 * @param vertex - point to get the color of
 * @param normal - normal of the surface
 * @param material - material of surface
 * @param start - starting point of the ray
 * @param groups - all the surfaces in the world
 * @param depth - depth of recursion
 * @ret - The color of the point
*/
RGB ImgDrawer::point_reflectance(Ray& ray, Vector vertex, Vector& normal, Face *face, Vector& start, map<string, Group>& groups, int depth){
	Vector sum;
	//if(depth > 0)
	//	return RGB(255,255,255);
	//return RGB(255,255,255);
	Material material = materials[face->get_material()];

	Vector v = Vector(vertex, start);
	v.normalize();
	if(normal * v < 0){
		normal = normal*-1;
	}
	Vector n = normal.normalize();

	Vector r = 2*(v*n)*n - v;
	r.normalize();

	for(unsigned i = 0; i<lights.size(); i++){
		Vector l = Vector(vertex, lights[i].source());
		l.normalize();

		if(!self_shadow(normal, l)){
			if(!external_shadow(vertex, lights[i], groups, face)){
//cout<<"Normal: "<<normal<<endl;
				double color[4];
				RGB c = lights[i].get_color();
				color[0] = c.r;
				color[1] = c.g;
				color[2] = c.b;
				color[3] = 1;
//cout<<color[0]<<" "<<color[1]<<" "<<color[2]<<endl;
				//lambertion
				material.get_LRM() * color;
//cout<<color[0]<<" "<<color[1]<<" "<<color[2]<<endl;
//cout<<"L "<<l<<endl;
//cout<<"Normal: "<<normal<<endl;
//cout<<"N*L: "<<normal*l<<endl;
	//			if(normal * l < 0){
					//return RGB(0, 0, 0);
	//				continue;
	//			}

//cout<<"sum: "<<sum<<endl;
				sum = sum + (Vector(color[0], color[1], color[2]) * (normal * l));
//cout<<"sum: "<<sum<<endl;

				//specular reflectance
				Vector col = Vector( c.r, c.g, c.b) * material.get_spec_reflect();
//cout<<"col "<<col<<endl;
				//col.normalize();

//cout<<"v "<<v<<endl;

				Vector rl = 2*(l*n)*n - l;
				rl.normalize();
//cout<<"rl "<<rl<<endl;
//REFRACT HERE if not refract check rv < 0 else dont
				double rlv = rl*v;
//cout<<"rlv "<<rlv<<" "<<(rlv<0.0001)<<endl;

				if(rlv < 0.0001){
					//return RGB(0, 0, 0);
					continue;
				}

				rlv = pow(rlv, material.get_phong_reflect());
//cout<<"rv "<<rv<<endl;
//cout<<"sum: "<<sum<<endl;
				sum = sum + (col*rlv);
//cout<<"sum: "<<sum<<endl;
			}//external shadow
		}//self shadow


	}//for

	Ray specRay(vertex, vertex + r);
//cout<<"reflected\n";
	RGB indirect_spec = ray_reflectance(specRay, groups, depth+1, face);
//cout<<"Ray: "<< vertex <<" + t"<<r<<endl;
	if((int)indirect_spec.r != 0 && (int)indirect_spec.g != 0 && (int)indirect_spec.b != 0){
//cout<<"Depth: "<<depth<<" Indirect specular: "<<material.get_spec_reflect()<<" * "<<(int)indirect_spec.r<<" "<<(int)indirect_spec.g<<" "<<(int)indirect_spec.b<<endl;
		sum[0] += material.get_spec_reflect()*indirect_spec.r;
		sum[1] += material.get_spec_reflect()*indirect_spec.g;
		sum[2] += material.get_spec_reflect()*indirect_spec.b;
//cout<<"sum: "<<sum<<endl;
	}

	if(material.get_translucency() != 0){
		Ray trans(vertex, ray.get_u() + vertex);
//cout<<"Trans "<<trans.get_start()<<" + t"<<trans.get_u()<<endl;
		RGB trans_color = ray_reflectance(trans, groups, depth+1, face);
		sum[0] += material.get_translucency()*trans_color.r;
		sum[1] += material.get_translucency()*trans_color.g;
		sum[2] += material.get_translucency()*trans_color.b;
//cout<<"sum: "<<sum<<endl;
	}



	if(sum[0] > 255){
		sum[0] = 255;
	}
	if(sum[1] > 255){
		sum[1] = 255;
	}
	if(sum[2] > 255){
		sum[2] = 255;
	}

	if(sum[0] < 0 || sum[1] < 0 || sum[2] < 0){
		throw "An error has occured while determining the color for a vertex\n";
	}
	return RGB(sum[0], sum[1], sum[2]);
}//point_reflectance

bool ImgDrawer::external_shadow(Vector& vertex, LightSource& light, map<string, Group>& groups, Face* ignore_face){
	Ray ptL(vertex, light.source());
//cout<<"Ray "<<ptL.get_start()<<" + t"<<ptL.get_u()<<endl;
//cout<<"Normal "<<ignore_face->get_normal()<<endl;
	for(map<string, Group>::iterator git = groups.begin(); git != groups.end(); ++git){
		for(vector<Face>::iterator fit = git->second.get_faces().begin(); fit != git->second.get_faces().end(); ++fit){
			if(&(*fit) == ignore_face)
				continue;
			double s = ptL.intersect(*fit);
			if( s > 0 && s < 1 )
				return true;
		}//for face
	}//for groups
	return false;
}//external_source

bool ImgDrawer::self_shadow(Vector& n, Vector& l){
	if(n*l < 0.0001)
		return true;
	return false;
}
