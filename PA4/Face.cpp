/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Face class file
 * 		Face class that contains a list of vertices
*/

#include "Face.h"

using std::string;
using std::vector;
using std::stringstream;

/*
 * Constructor
*/
Face::Face(VertexList& v, string& use_mat): v_list(&v), material(use_mat){}

/*
 * Addes the index of a vertex to the face's vertex list
 * @param index - The index of the vertex in some static list of vertices
*/
bool Face::add_vertex(unsigned index){
	index_list.push_back(index);
	return true;
}

/*
 * Checks to see if the face is valid
 * Throws a std::string exception if the face is not vaild
*/
void Face::check_face(){
	if(index_list.size() <3){
		stringstream e;
		e << "An error has occured. Face f ";
		e << index_list[0] << " " << index_list[1];
		e << " Only contains two vertices. Three are required.\n";
		throw e.str();
	}
	
	for(;;){
		if(index_list.size() < 3){
			stringstream e;
			e << "An error has occured. Face f ";
			for(unsigned i=0; i<index_list.size();i++)
				e << index_list[i] << " ";
			e << " Has all vertices that are colinear.\n";
			throw e.str();	
		}
		//creates the surface normal vector with 
		//point 0 and 1 and 1 and 2
		Vector ab( (*v_list)[index_list[0]], (*v_list)[index_list[1]] );
		Vector bc( (*v_list)[index_list[1]], (*v_list)[index_list[2]] );

		normal = create_normal(ab, bc);

		if(normal.magnatude() != 0) break;

		vector<unsigned>::iterator it = index_list.begin();
		index_list.erase(++it);//always remove the index at pos 1
	}
	
	normal = normal.normalize();

	for(unsigned i = 1; i <= index_list.size(); i++){
		Vector next( (*v_list)[index_list[i-1]], (*v_list)[index_list[i%index_list.size()]] );
		double dot = normal*next;
		if(dot >= 0.01 || dot <= -0.01){
			stringstream e;
			e << "An error has occured. Face f ";
			for(unsigned i=0; i<index_list.size();i++)
				e << index_list[i] << " ";
			e << " Is not coplaner.\n";
			throw e.str();	
		}
	}


	Vector ab( (*v_list)[index_list[index_list.size()-1]], (*v_list)[index_list[0]] );
	Vector bc( (*v_list)[index_list[0]], (*v_list)[index_list[1]] );
	Vector prev = create_normal(ab, bc);
	for(unsigned i = 1; i<index_list.size()-2; i++){
		ab = bc;
		bc = Vector((*v_list)[index_list[i]], (*v_list)[index_list[i+1]]);
		Vector next = create_normal(ab, bc);
		double dot = prev * next;
		if(dot <= -0.1){//can't be negative??
			stringstream e;
			e << "An error has occured. Face f";
			for(unsigned i=0; i<index_list.size();i++)
				e << " " << index_list[i];
			e << ". Is not convex.\n";
			throw e.str();	
	
		}
		prev = next;
	}
}

vector<Face> Face::make_triangular(){
	vector<Face> faces;
	if(index_list.size() == 3){
		faces.push_back(*this);
		return faces;
	}
		
	double sum[4] = {0.0, 0.0, 0.0, 1.0};
	for(unsigned i=0; i<index_list.size(); i++){
		sum[0] += (*v_list)[index_list[i]][0];
		sum[1] += (*v_list)[index_list[i]][1];
		sum[2] += (*v_list)[index_list[i]][2];
	}
	sum[0] /= index_list.size();
	sum[1] /= index_list.size();
	sum[2] /= index_list.size();
	(*v_list).add_vertex(sum[0], sum[1], sum[2], sum[3]);
	unsigned last = (*v_list).size();
	for(unsigned i=0; i<index_list.size(); i++){
		Face f((*v_list), material);
		f.add_vertex(index_list[i]);
		f.add_vertex(last);
		f.add_vertex(index_list[(i+1)%index_list.size()]);
		f.normal = normal;
		//f.check_face();
		faces.push_back(f);
	}
	return faces;
}

/*
 * Creates a string of the face's data in .obj format
 * @ret - A std::string in .obj format for the Face
*/
string Face::to_obj(){
	stringstream ss;
	ss<<"f ";
	
	vector<unsigned>::iterator it = index_list.begin();
	for(; it != index_list.end(); ++it){
		ss<<*it<<" ";
	}
	return ss.str();
}

/*
 * Returns a list of indices for the vertices in the Face
 * @ret - A vector of unsigned integers that are indices into a static list of vertices
*/
vector<unsigned>& Face::get_vertices(){
	return index_list;
}

/*
 * Recompute normals for a face
*/
void Face::compute_normal(){
	//creates the surface normal vector with 
	//point 0 and 1 and 1 and 2
	Vector ab( (*v_list)[index_list[0]], (*v_list)[index_list[1]] );
	Vector bc( (*v_list)[index_list[1]], (*v_list)[index_list[2]] );

	normal = create_normal(ab, bc);
	normal.normalize();
}
