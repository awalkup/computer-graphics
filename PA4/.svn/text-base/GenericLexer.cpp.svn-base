/* 
 * Austin Walkup
 * Aug 30, 2011
 *
 * GenericLexer 
 * 		GenericLexer is a full lexer however is does not contain a grammer
 * 		therefore you cannot create a GenericLexer you must inherit from it,
 * 		create the getToken method and define the grammer using the inherited method:
 * 		'reserve(std::string, Token)'
*/

#include "GenericLexer.h"

using std::string; 
using std::stringstream;

/**
 * Constructor
*/
GenericLexer::GenericLexer(string filename): line(1), pos(1), in(filename.c_str()){
	if(!in)
		throw "Unable to open file "+filename;
}

/**
 * Deconstructor
*/
GenericLexer::~GenericLexer(){
	in.close();
}

/**
 * Makes the string 'key' a reserved word for the lexer
 * @param key - The string to reserve
 * @param value - The Token that the string represents in yoyr grammar
 *
*/
void GenericLexer::reserve(string key, Token::Tag value){
	lex_map[key] = value;
}

/**
 * Finds the Token corresponding to 'key' if it exists
 * @param key - The string corresponding to the Token to be found
 * @ret - Token that corresponds to 'key' if it exists, NULL otherwise
*/
Token::Tag GenericLexer::lookup(const string &key){
	if(lex_map.find(key) == lex_map.end())
		return Token::NONE;

		return lex_map[key];
}

/**
 * Gets the next set of values seperated by whitespace, returns newlines when encountered alone
 * @ret - String that was the next whitspace seperated values in the stream
*/
string GenericLexer::next_word(){
	stringstream next("");

	int c = in.get();
	if (in.eof())
		return "END_OF_FILE";

	//skips all leading whitespace
	while(isspace(c) && !(in.peek() == EOF)){
		if( (char)c == '\n' || (char)c == '\r'){
			line++;
			pos = 0;
			return "\n";
		}
		pos++;

		c = in.get();
	}

	do { /*checks if the value c is a whitespace char */
		pos++;
		next << (char)c;

		if((char)in.peek() == '\n' || (char)in.peek() == '\r')
			return next.str();
	
		c = in.get();
	} while( !isspace(c) && !(in.peek() == EOF));
	return next.str();
}

