/*
 * Austin Walkup
 * Oct 10, 2011
 *
 * ImgAlgorithm header file
 * 		All the algorithms for creating an image are here. Call draw
*/

#ifndef __IMG_ALGS_H__WALKUP__
#define __IMG_ALGS_H__WALKUP__

#include <vector>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <cfloat>
#include <cmath>

#include "VertexList.h"
#include "Vector.h"
#include "Matrix.h"
#include "Group.h"
#include "Face.h"
#include "Material.h"
#include "LightSource.h"
#include "Camera.h"
#include "Point.h"

#define round(d) (d)>0 ? ((int)((d)+0.5)) : ((int)((d)-0.5))


class ImgDrawer {
	public:
		ImgDrawer(double, double, double, double);
		ImgDrawer(double, double, double, double, vector<LightSource>&, map<string, Material>&);
		std::vector<std::vector<RGB> > draw_wireFrame(Matrix&,std::map<std::string, Group>&, VertexList);
		std::vector<std::vector<RGB> > draw_surfaceRender(Camera&,std::map<std::string, Group>&, VertexList);
		std::vector<std::vector<RGB> > draw_raytrace(Camera&,std::map<std::string, Group>&, VertexList);
	private:
		void set_pixel(int, int, RGB color = RGB(255, 255, 255));
		bool check_pixel(int, int, double);
		void bresenhams(int, int, int, int);
		void bresenhams(int, unsigned char, int, unsigned char, unsigned char*);
		void bresenhams(int, double, int, double, double*);
		bool clipping(int&, int&, int&, int&);
		int clipping_helper(int, int);
		void polygon_filling(std::multiset<Point>&);
		void scanLine_intersection(std::multiset<Point>&, double, double, double, double, Point&, Point&);
		RGB calculate_vertex_color(double*, Vector&, Material&, Vector&);
		void interpolate(int, int, const Point&, const Point&, unsigned char*, unsigned char*, unsigned char*, double*);
		RGB ray_reflectance(Ray&, std::map<std::string, Group>&);
		
		double minx;
		double miny;
		double maxx;
		double maxy;
		std::vector<std::vector<RGB> > img;
		std::vector<std::vector<double> > z_buf;
		std::vector& lights;
		std::map<string, Material>& materials;
		/*
		 * For clipping
		*/
		const static int INSIDE = 0; // 0000
		const static int LEFT = 1;   // 0001
		const static int RIGHT = 2;  // 0010
		const static int BOTTOM = 4; // 0100
		const static int TOP = 8;    // 1000
};

#endif /* __IMG_ALGS_H__WALKUP__ */
