/*
 * Austin Walkup
 * Oct 23, 2011
 *
 * Material header file
 * 		Class that contains the material properties for a Face
*/

#ifndef __MATERIAL_H__WALKUP__
#define __MATERIAL_H__WALKUP__

#include <string>
#include <sstream>
#include "Matrix.h"

class Material {
	public:
		Material();
		Material(std::string&);
		Material(std::string, double, double, double, double, double, double);
		Material(const Material&);

		Material& operator=(const Material&);

		std::string& get_name(){ return name; }
		double get_spec_reflect(){ return spec_reflect; }
		double get_phong_reflect(){ return phong_reflect; }
		double get_translucency() { return translucency; }
		Matrix& get_LRM(){ return lrm; }

	private:
		void create_LRM(double, double, double);
		std::string name;
		double spec_reflect;
		double phong_reflect;
		double translucency;
		Matrix lrm;
};

#endif /* __METERIAL_H__WALKUP__ */
