/*
 * Austin Walkup
 *
 * Nov 23, 2011
 * 
 * Ray class file
 * 		Class that implements a ray
*/

#include "Ray.h"

Ray::Ray() {}

/*

*/
Ray::Ray(Vector& start, Vector end): start(start), end(end), u(end - start) {}

/*
 * Finds the distance along the ray where it intersects the triangle 
 * @param face - The triangle to find the intersection with
 * @ret - Double that is the distance down the ray, -1 if there is no intersection
*/
#include<iostream>
double Ray::intersect(Face& face){
//std::cout<<"Ray: "<<start<<" + t"<<u<<"    end: "<<end<<std::endl;
	double* a = face.a();
	double* b = face.b();
	double* c = face.c();

	Vector n = face.get_normal();
//std::cout<<"Normal: "<<n<<std::endl;
	Vector w0(start[0]-a[0], start[1] - a[1], start[2]-a[2]);
//std::cout<<"w0: "<<w0<<std::endl;
	double p = -(n * w0);
//std::cout<<"p: "<<p<<std::endl;
	double q = n * u;
//std::cout<<"q: "<<q<<std::endl;
	if(std::abs(q) < 0.00001){
		return 0;
	}

	double t = p/q;
//std::cout<<"t: "<<t<<std::endl;
	if(t < 0.000001)
		return 0;

	Vector pt = (*this)[t];
//std::cout<<"Pt: "<<pt<<std::endl;

	Vector w(pt[0] - a[0], pt[1] - a[1], pt[2] - a[2]);
//std::cout<<"w: "<<w<<std::endl;

	Vector u(b[0] - a[0], b[1] - a[1], b[2] - a[2]);
//std::cout<<"u: "<<u<<std::endl;

	Vector v(c[0] - a[0], c[1] - a[1], c[2] - a[2]);
//std::cout<<"v: "<<v<<std::endl;

	double denom = (u*v) * (u*v) - (u*u) * (v*v);
//std::cout<<"denom: "<<denom<<std::endl;
	double beta = ( (u*v) * (w*v) - (v*v) * (w*u) ) / denom;
//std::cout<<"beta: "<<beta<<std::endl;
	double gamma = ( (u*v) * (w*u) - (u*u) * (w*v) ) / denom;
//std::cout<<"gamma: "<<gamma<<std::endl;

	// ray does not intersect the triangle
	if(beta < 0 || gamma < 0 || beta + gamma > 1){
		return 0;
	}

	return t;
}//intersect

/*
 * Finds the point along the ray with a distance
 * @param distance - The distance along the ray
 * @ret - Vector that is 'distance' along the ray
*/
Vector Ray::operator[](double distance) {
	return start + (Vector(u)*distance);
}
