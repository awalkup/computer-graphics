/*
 * Austin Walkup
 * Sep 11, 2011
 *
 * Matrix header file
 * 		Matrix class for transformations on data
*/

#ifndef __MATRIX_H__WALKUP__
#define __MATRIX_H__WALKUP__

#include <iostream>
#include <algorithm>
#include <cmath>
#include "Vector.h"

class Matrix {
	public:
		//arbatrary matrix
		Matrix( double, double, double, double,
				double, double, double, double,
				double, double, double, double,
				double, double, double, double);
		Matrix();
		Matrix(const Matrix&);
		~Matrix();
		Matrix& operator=(const Matrix&);

		int get_size();
		Matrix operator*(Matrix&);
		void operator*(double*);
		Matrix& transpose();
		void multiply_ignore_z(double*);
		double det();
		friend std::ostream& operator<<(std::ostream&, const Matrix&);
	private:
		void create_matrix( double, double, double, double,
							double, double, double, double,
							double, double, double, double,
							double, double, double, double);
		int size;
		double matrix[4][4];
};
std::ostream& operator<<(std::ostream&, const Matrix&);

Matrix create_rotate(double, double, double, double);
Matrix create_scale(double, double, double);
Matrix create_scale(double);
Matrix create_translate(double, double, double);

#endif /* __MATRIX_H__WALKUP__ */
