
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include "Parser.h"
#include "CmdParser.h"
#include "Lexer.h"
#include "CmdLexer.h"
#include "Group.h"
#include "Face.h"
#include "VertexList.h"
#include "Matrix.h"
#include "ImgAlgorithms.h"
#include "Material.h"
#include "LightSource.h"

using namespace std;

void transform_data(map<string, Group>& groups, map<string, Matrix>& matrices, VertexList& v_list){

	map<string, Matrix>::iterator mats = matrices.begin();

	for(;mats != matrices.end(); ++mats){
		if(groups.find(mats->first) == groups.end())
			throw "Group "+mats->first+" does not exist";

		set<unsigned> vert_set = groups.find(mats->first)->second.get_vertices();
		set<unsigned>::iterator it = vert_set.begin();

		for(; it != vert_set.end(); ++it){
			mats->second * v_list[*it];
		}//for
	}//for
}//transform_data

int main(int argc, char **argv){
	try{
		if(argc < 4){
			cout<<"Usage:\n\t"<<argv[0];
			cout<<" <input.obj> <transform.cmd> <img_base_name>\n";
			return -1;
		}
		VertexList v_list;

		Lexer myLex(argv[1]);
		CmdLexer cmdLex(argv[2]);
		Parser parser(myLex);
		CmdParser cmdparser(cmdLex);

		map<string, Material> material_map;
		map<string, Group> groups = parser.parse_obj(v_list, material_map);
		int wireframe_ctr = 1;
		map<string, Camera> cameras;
		map<string, Matrix> matrices;
		vector<LightSource> lightSources;
		string wireFrame_camera;
		string surface_render;
		string raytrace;
		CmdData cmdData(cameras, material_map, lightSources, wireFrame_camera, surface_render, raytrace);
		do {
			cmdparser.parse_cmd(cmdData);
			if(wireFrame_camera != ""){
				matrices = cmdData.matrix_map;
				transform_data(groups, matrices, v_list);
				stringstream ss;
				ss <<argv[3];
				if(wireframe_ctr != 1){
					ss << "_" << wireframe_ctr;
				}
				Camera cam(cameras[wireFrame_camera]);
				ImgDrawer drawer(cam.get_minx(), cam.get_miny(),
							cam.get_maxx(), cam.get_maxy());

				vector<vector<RGB> > img = drawer.draw_wireFrame(cam, groups, v_list);
				cam.create_image(ss.str(), img);
				//for multiple wireframe commands
				wireframe_ctr++;
			}
			else if(surface_render != ""){
				matrices = cmdData.matrix_map;
				transform_data(groups, matrices, v_list);
				stringstream ss;
				ss <<argv[3];
				if(wireframe_ctr != 1){
					ss << "_" << wireframe_ctr;
				}
				Camera cam(cameras[surface_render]);
				ImgDrawer drawer(cam.get_minx(), cam.get_miny(),
							cam.get_maxx(), cam.get_maxy(), 
							lightSources, material_map);

				vector<vector<RGB> > img = drawer.draw_surfaceRender(cam, groups, v_list);
				cam.create_image(ss.str(), img);
				//for multiple wireframe commands
				wireframe_ctr++;
			}
			else if(raytrace != ""){
				matrices = cmdData.matrix_map;
				transform_data(groups, matrices, v_list);
				stringstream ss;
				ss <<argv[3];
				if(wireframe_ctr != 1){
					ss << "_" << wireframe_ctr;
				}
				Camera cam(cameras[raytrace]);
				ImgDrawer drawer(cam.get_minx(), cam.get_miny(),
							cam.get_maxx(), cam.get_maxy(),
							lightSources, material_map);

				vector<vector<RGB> > img = drawer.draw_raytrace(cam, groups);
				cam.create_image(ss.str(), img);
				//for multiple wireframe commands
				wireframe_ctr++;
			}

		} while(wireFrame_camera != "" && surface_render != "");

	}catch(std::string e ){
		cerr<<e<<endl;
		return -1;
	}catch(char const* e){
		cerr<<e<<endl;
		return -1;
	}

	return 0;
}
