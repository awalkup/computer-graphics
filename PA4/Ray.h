/*
 * Austin Walkup
 *
 * Nov 23, 2011
 * 
 * Ray header file
 * 		Class that implements a ray
*/

#ifndef __RAY_H__WALKUP__
#define __RAY_H__WALKUP__

#include <cmath>
#include "Vector.h"
#include "Face.h"

class Ray {
	public:
		Ray();
		Ray(Vector&, Vector);
		Vector& get_start() { return start; }
		Vector& get_u(){ return u; }
		double intersect(Face&);
		Vector operator[](double);

	private:
		Vector start;
		Vector end;
		Vector u;
};

#endif /* __RAY_H__WALKUP__ */
