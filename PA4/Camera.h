/*
 * Austin Walkup
 * Oct 5, 2011
 *
 * Camera header file
 * 		The representation of a camera (holds focal point, VPN, VUP, etc.)
*/

#ifndef __CAMERA_H__WALKUP__
#define __CAMERA_H__WALKUP__

#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include "Vector.h"
#include "Matrix.h"
#include "LightSource.h"

class Camera {
	public:
		enum Filetype{PPM};
		Camera();
		Camera(std::string, double, double, double,double, double, 
							double,double, double, double, double);
		// sets Image dimensions
		void imgPlane(double, double, double, double);
		void create_image(std::string, std::vector<std::vector<RGB> >&, Filetype = PPM);
		std::string get_name() { return name; }
		double get_focalLength() { return focalL; }
		Vector& get_focalP() { return focalP; }
		Vector get_n() { return n; }
		Vector get_u() { return u; }
		Vector get_v() { return v; }
		Matrix& get_proj_matrix() { return proj; }
		double get_minx(){ return minx; }
		double get_miny(){ return miny; }
		double get_maxx(){ return maxx; }
		double get_maxy(){ return maxy; }
		double sizex(){ return maxx - minx; }
		double sizey(){ return maxy - miny; }

	private:
		void create_proj(double, double, double,
						 double, double, double);

		std::string name;
		Vector focalP;
		double focalL;
		Matrix proj;
		Vector n;
		Vector u;
		Vector v;

		// Image dimensions
		double minx;
		double miny;
		double maxx;
		double maxy;
};

#endif /* __CAMERA_H__WALKUP__ */
