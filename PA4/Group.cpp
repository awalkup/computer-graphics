/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Group class file
 * 		Group class that contains a list of faces
*/

#include "Group.h"

using std::string;
using std::stringstream;
using std::vector;
using std::set;


/*
 * Constructor
 * @param name - Name of the Group
*/
Group::Group(string name): name(name) {}

/*
 * Constructor
 * @param name - Name of the Group
 * @param f1 - The first face of the group
 * @param f2 - The second face of the group
 * @param f3 - The third face of the group
*/
Group::Group(string name, Face f1, Face f2, Face f3): name(name) {
	vector<Face> f;
	f.push_back(f1);
	f.push_back(f2);
	f.push_back(f3);
	addFace(f);
}//Group

/*
 * Addes a Face to the Group
 * @param face - the face to add
*/
void Group::addFace(vector<Face> faces){
	for(unsigned i=0; i<faces.size(); i++){
		face_list.push_back(faces[i]);
		vector<unsigned> f_vec = faces[i].get_vertices();
		vector<unsigned>::iterator it = f_vec.begin();
		for(; it != f_vec.end(); ++it){
			add_vertex(*it);
		}
	}
}

/*
 * Addes a Face to the Group
 * @param face - the face to add
*/
void Group::addFace(Face face){
	addFace(vector<Face>(1, face));
}


/*
 * Checks the Group to make sure that there are no errors in the faces
 * i.e. Verifies that the faces in the group don't reference a vertex
 * from another group, that the polygon formed from the faces is convex 
 * and that each face isn't significantly non-coplaner
 * @throws std::string - Throws a std::string exception if there is an error
*/
void Group::check_group(){
	vector<Face>::iterator it = face_list.begin();
	vector<Face> new_faces;

	for(; it != face_list.end(); ++it){
		it->check_face();
		vector<Face> f = it->make_triangular();
		for(unsigned i = 0; i<f.size(); i++){
			new_faces.push_back(f[i]);
		}
	}
	face_list.clear();
	grp_vertices.clear();
	this->addFace(new_faces);
}

/*
 * Creates a string in .obj format for the group it's faces and their vertices
 * @param v_list - A static list that contains all vertices in the system
 * @ret - A string in .obj format for the Group
*/
string Group::to_obj(){
	stringstream ss;
	if(name != "default")//default needs no heading
		ss<<"g "<<name<<'\n';//creates the group heading
	
	for(vector<Face>::iterator it = face_list.begin(); 
								it != face_list.end(); ++it) {
		ss << it->to_obj() << '\n';//addes each face's info to the string
	}
	return ss.str();
}

/*
 * Returns the name of the Group
 * @ret - std::string that is the name of the Group
*/
string Group::get_name(){
	return name;
}

/*
 * Gets a set of all vertices that belong to this group
 * @ret - A set that are the indices of the vertices in the group
*/
set<unsigned>& Group::get_vertices(){
	return grp_vertices;
}

/*
 * Sets the name of the group
*/
void Group::set_name(string newName){
	name = newName;
}

/*
 * Addes all the vertices from a face to the group's list of vertices
 * @param face - The Face who's vertices are being added
*/
void Group::add_vertex(unsigned pos){
	if(grp_vertices.find(pos) == grp_vertices.end())
		grp_vertices.insert(pos);
}

