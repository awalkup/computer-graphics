/*
 * Austin Walkup
 * Nov 21, 2011
 * 
 * Point header file
 *		Class that defines a Point
*/


#ifndef __POINT_H__WALKUP__
#define __POINT_H__WALKUP__

typedef struct Point {
    Point(): x(0), y(0), color(-1, -1, -1), depth(0) {}
    Point(int x, int y, RGB color, double d): x(x), y(y), color(color.r, color.g, color.b), depth(d) {}
    bool operator<(const Point& rhs) const {
        if(y < rhs.y)
            return true;
        else if(y == rhs.y){
            if(x < rhs.x)
                return true;
        }   
        return false;
    }   
    int x;
    int y;
    RGB color;
    double depth;
} Point;

#endif /* __POINT_H__WAlKUP__ */
