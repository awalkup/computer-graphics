/*
 * Austin Walkup
 * Oct 11, 2011
 *
 * ImgAlgorithms class file
 * 		All the algorithms for creating an image are here. Call draw
*/

#include "ImgAlgorithms.h"

using std::vector;
using std::string;
using std::map;

		#include<iostream>
/*
 * Constructor
*/
ImgDrawer::ImgDrawer(double xmin, double ymin, double xmax, double ymax): 
			 minx(xmin), miny(ymin), maxx(xmax), maxy(ymax),
			 img( round((ymax - ymin))+1, vector<rgb>(round((xmax - xmin))+1)) {}

/*
 * Draws all objects from 'groups'
 * @param groups - The list of objects to draw
 * @param v_list - The list of vertices the objects refer to (pass by value so that data isn't lost)
 * @ret - A vector of rgb values, size of image_width * image_height
*/
vector<vector<rgb> > ImgDrawer::draw(Matrix& proj, map<string, Group>& groups, VertexList v_list){
	for(unsigned i=1; i<=v_list.size(); i++){
		proj * v_list[i];
		//v_list.w_normalize(i);
	}

	for(map<string, Group>::iterator it = groups.begin(); it != groups.end(); ++it){
		vector<Face> f_vec = it->second.get_faces();
		for(unsigned k=0; k<f_vec.size(); k++){
			vector<unsigned> fv_vec = f_vec[k].get_vertices();
			int x1 = round(v_list[ fv_vec[ fv_vec.size()-1 ] ][0]);
			int y1 = round(v_list[ fv_vec[ fv_vec.size()-1 ] ][1]);
	
			for(unsigned i=0; i< fv_vec.size(); ++i){
	
				int x2 = round(v_list[fv_vec[i]][0]);
				int y2 = round(v_list[fv_vec[i]][1]);
				clipping(x1, y1, x2, y2);
	
				x1 = x2;
				y1 = y2;
			}//for vertices
		}//for faces
	}//for groups
	return img;
}//draw

/*
 * Code inspired by http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
 * Preforms Bresenham's to find what points to draw
 * @param x1 - x-coordinate of end-point 1
 * @param y1 - y-coordinate of end-point 1
 * @param x2 - x-coordinate of end-point 2
 * @param y2 - y-coordinate of end-point 2
*/
void ImgDrawer::bresenhams(int x1, int y1, int x2, int y2){
	rgb color;
	color.r = 255;
	color.g = 255;
	color.b = 255;


	int dx = (x2 - x1);
	dx = (dx > 0) ? dx : -dx;
	int dy = (y2 -y1);
	dy = (dy > 0) ? dy : -dy;

	int sx = (x1 < x2) ? 1 : -1;
	int sy = (y1 < y2) ? 1 : -1;

	int err = dx - dy;

	do {
		img[x1][y1] = color;

		int e2 = 2*err;
		if(e2 > -dy){
			err -= dy;
			x1 += sx;
		}
		if(e2 < dx){
			err += dx;
			y1 += sy;
		}
	} while(x1 != x2 || y1 != y2);
}

/*
 * Code inspired by http://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
 * Cohen-Sutherland clipping algorithm, clips the line to be drawn to the field of view
 * then calls breshenhams to draw the line
 * @param x1 - x-coordinate of end-point 1
 * @param y1 - y-coordinate of end-point 1
 * @param x2 - x-coordinate of end-point 2
 * @param y2 - y-coordinate of end-point 2
*/
void ImgDrawer::clipping(int x1, int y1, int x2, int y2){
	int p1_code = clipping_helper(x1,y1);
	int p2_code = clipping_helper(x2,y2);

	bool accept = false;
	for(;;){
		if( !(p1_code | p2_code) ){ // Bitwise OR is zero, accept
			accept = true;
			break;
		}
		else if( p1_code & p2_code ) // Bitwise AND is non_zero, reject
			break;
		else {
			int x, y;
			
			//one of the points out of bounds
			int out_code = p1_code ? p1_code : p2_code;

			// Find the intercept
			if(out_code & TOP){
				x = x1 + (x2 - x1) * (maxy - y1) / (y2 - y1);
				y = maxy;
			}
			else if(out_code & BOTTOM){
				x = x1 + (x2 - x1) * (miny+1 - y1) / (y2 - y1);
				y = miny+1;
			}
			else if(out_code & RIGHT){
				x = maxx-1;
				y = y1 + (y2 - y1) * (maxx-1 - x1) / (x2 - x1);
			}
			else if(out_code & LEFT){
				x = minx;
				y = y1 + (y2 - y1) * (minx - x1) / (x2 - x1);
			}
			else {
				throw "An error has occured while finding clipping intercepts";
			}

			if(out_code == p1_code){
				x1 = x;
				y1 = y;
				p1_code = clipping_helper(x1,y1);
			}
			else if(out_code == p2_code){
				x2 = x;
				y2 = y;
				p2_code = clipping_helper(x2,y2);
			}
			else {
				throw "An error has occured while clipping";
			}
		}//else
	}//for

	if(accept){
		bresenhams(-(y1+miny), x1-minx, -(y2+miny), x2-minx);
		//bresenhams(x1-minx, -(y1+miny), x2-minx, -(y2+miny));
	}
}//clipping

/*
 * Helper method for the clipping algorithm
 * @param x - x-coordinate of end-point
 * @param y - y-coordinate of end-point
 * @ret - an int that is a bit string for the areas where the endpoint violates
*/
int ImgDrawer::clipping_helper(int x, int y){
	int code = INSIDE;
	
	if(x < minx)
		code |= LEFT;
	else if(x >= maxx)
		code |= RIGHT;

	if(y <= miny)
		code |= BOTTOM;
	else if(y > maxy)
		code |= TOP;

	return code;
}
