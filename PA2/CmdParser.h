/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * CmdParser header file
 * 		Parser that has methods to parse a command file 
 *
*/

#ifndef __CMD_PARSER_H__WALKUP__
#define __CMD_PARSER_H__WALKUP__

#include <string>
#include <sstream>
#include <map>
#include <algorithm>
#include "Token.h"
#include "CmdLexer.h"
#include "Matrix.h"
#include "Camera.h"

typedef struct CmdData {
	std::map<std::string, Matrix> matrix_map;
	std::map<std::string, Camera> camera_map;
	std::string wireFrame_camera;
} CmdData;


class CmdParser {
	public:
		CmdParser(CmdLexer&);
		//default destructor fine
		CmdData parse_cmd(std::map<std::string, Camera>&);

	private:
		void parse_numbers(double*, int);
		void unexpectedToken(Token, std::string);
		void generic_error(Token, std::string);
		void create_rmatrix(std::map<std::string, Matrix>&);
		void create_smatrix(std::map<std::string, Matrix>&);
		void create_tmatrix(std::map<std::string, Matrix>&);
		void create_amatrix(std::map<std::string, Matrix>&);
		void create_camera(std::map<std::string, Camera>&);
		std::string create_wireFrame(std::map<std::string, Camera>&);
		CmdLexer &lex;
};

#endif /* __CMD_PARSER_H__WALKUP__ */
