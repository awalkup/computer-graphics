/*
 * Austin Walkup
 * Oct 10, 2011
 *
 * ImgAlgorithm header file
 * 		All the algorithms for creating an image are here. Call draw
*/

#ifndef __IMG_ALGS_H__WALKUP__
#define __IMG_ALGS_H__WALKUP__

#include <vector>
#include <map>
#include <string>
#include "VertexList.h"
#include "Matrix.h"
#include "Group.h"
#include "Face.h"

#define round(d) (d)>0 ? ((int)((d)+0.5)) : ((int)((d)-0.5))

typedef struct {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} rgb;

class ImgDrawer {
	public:
		ImgDrawer(double, double, double, double);
		std::vector<std::vector<rgb> > draw(Matrix&,std::map<std::string, Group>&, VertexList);
	private:
		void bresenhams(int, int, int, int);
		void clipping(int, int, int, int);
		int clipping_helper(int, int);
		
		double minx;
		double miny;
		double maxx;
		double maxy;
		std::vector<std::vector<rgb> > img;
		/*
		 * For clipping
		*/
		const static int INSIDE = 0; // 0000
		const static int LEFT = 1;   // 0001
		const static int RIGHT = 2;  // 0010
		const static int BOTTOM = 4; // 0100
		const static int TOP = 8;    // 1000
};

#endif /* __IMG_ALGS_H__WALKUP__ */
