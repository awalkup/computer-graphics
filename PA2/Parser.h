/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Parser header file
 * 		Parser that has methods to parse an obj file
 *
*/

#ifndef __PARSER_H__WALKUP__
#define __PARSER_H__WALKUP__

#include <string>
#include <sstream>
#include <map>
#include <set>
#include <algorithm>
#include "Token.h"
#include "Lexer.h"
#include "VertexList.h"
#include "Face.h"
#include "Group.h"

class Parser {
	public:
		Parser(Lexer&);
		//default destructor fine
		std::map<std::string, Group> parse_obj(VertexList&);

	private:
		void check_integrity(std::map<std::string, Group>&);
		void unexpectedToken(Token, std::string);
		void generic_error(Token, std::string);
		Group create_group(std::map<std::string, Group>&);
		Face create_face(VertexList&);
		unsigned create_vertex(VertexList&);
		Lexer &lex;
};

#endif /* __PARSER_H__WALKUP__ */
