
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include "Parser.h"
#include "CmdParser.h"
#include "Lexer.h"
#include "CmdLexer.h"
#include "Group.h"
#include "Face.h"
#include "VertexList.h"
#include "Matrix.h"
#include "ImgAlgorithms.h"

using namespace std;

void transform_data(map<string, Group>& groups, map<string, Matrix>& matrices, VertexList& v_list){

	map<string, Matrix>::iterator mats = matrices.begin();

	for(;mats != matrices.end(); ++mats){
		if(groups.find(mats->first) == groups.end())
			throw "Group "+mats->first+" does not exist";

		set<unsigned> vert_set = groups.find(mats->first)->second.get_vertices();
		set<unsigned>::iterator it = vert_set.begin();

		for(; it != vert_set.end(); ++it){
			mats->second * v_list[*it];
		}//for
	}//for
}//transform_data

int main(int argc, char **argv){
	try{
		if(argc < 4){
			cout<<"Usage:\n\t"<<argv[0];
			cout<<" <input.obj> <transform.cmd> <img_base_name>\n";
			return -1;
		}
		VertexList v_list;

		Lexer myLex(argv[1]);
		CmdLexer cmdLex(argv[2]);
		Parser parser(myLex);
		CmdParser cmdparser(cmdLex);

		map<string, Group> groups = parser.parse_obj(v_list);
		int wireframe_ctr = 1;
		CmdData cmdData;
		map<string, Camera> cameras;
		do {
			cmdData = cmdparser.parse_cmd(cameras);
			map<string, Matrix> matrices = cmdData.matrix_map;
			cameras = cmdData.camera_map;
			if(cmdData.wireFrame_camera != ""){
				transform_data(groups, matrices, v_list);
				stringstream ss;
				ss <<argv[3];
				if(wireframe_ctr != 1){
					ss << "_" << wireframe_ctr;
				}
				Camera cam(cameras[cmdData.wireFrame_camera]);
				ImgDrawer drawer(cam.get_minx(), cam.get_miny(),
							cam.get_maxx(), cam.get_maxy());

				vector<vector<rgb> > img = drawer.draw(cam.get_proj_matrix(), groups, v_list);
				cam.create_image(ss.str(), img);
				//for multiple wireframe commands
				wireframe_ctr++;
			}
		} while(cmdData.wireFrame_camera != "");

	}catch(std::string e ){
		cerr<<e<<endl;
		return -1;
	}

	return 0;
}
