/* 
 * Austin Walkup
 * Aug 30, 2011
 *
 * Token class file
 * 		Class for defining the grammar for Generic Lexer
 *
*/

#include "Token.h"

using std::string;

/**
 * Default Constructor with no parameters (Dummy Token)
*/
Token::Token(): line(0), pos(0), tag(NONE), value("") {}

/**
 * Constructor that sets the values for the class
 * @param line - The line the Token was found on
 * @param pos - The position in the line that the Token was found at (starting position)
 * @param tag - The enum tag for this Token
 * @param value - optional parameter for any extra data the Token has associated with it
*/
Token::Token(int l, int p, Tag t, string v): line(l),pos(p),
	tag(t),value(v) {}

/**
 * Represents the Token as a string
 * @ret - A string that represents the Token
*/
Token::operator std::string(){
	if(tag == VERTEX){
		return "<VERTEX>";
	}

	else if(tag == FACE){
		return "<FACE>";
	}

	else if(tag == GROUP){
		return "<GROUP>";
		
	}
	else if(tag == COMMENT){
		
		return "<COMMENT>";
	}

	else if(tag == ID){
		
		return "<ID>";
	}

	else if(tag == FLOAT){
		
		return "<FLOAT>";
	}

	else if(tag == NEWLINE){
		
		return "<NEWLINE>";
	}

	else if(tag == INTEGER){
		
		return "<INTEGER>";
	}

	else if(tag == END_OF_FILE){
		return "<EOF>";
		
	}

	else{

		return "<NONE>";
	}
}

/*
 * Compare to operator
 * @param t - A tag to compare the current Token's tag to
 * @ret - True if the tags are the same, false otherwise
*/
bool Token::operator==(Tag t){
	return this->tag == t;
}

/*
 * Compare to operator
 * @param token - A token to compare the current Token with
 * @ret - True if the Token's tags are the same, false otherwise
*/
bool Token::operator==(Token token){
	return *this == token.tag;
}

/*
 * Not equal operator
 * @param t - A tag to compare the current Token's tag to
 * @ret - True if the tags are not the same, false otherwise
*/
bool Token::operator!=(Tag t){
	return !(*this == t);
}

/*
 * Not equal operator
 * @param token - A Token to compare the current Token with
 * @ret - True if the Token's tags are not the same, false otherwise
*/
bool Token::operator!=(Token token){
	return !(*this == token);
}

/**
 * Gets any additional data that is associated with this Token
 * @ret - A string that contains any data associated with this Token
*/
string Token::get_data(){
	return value;
}

