/* 
 * Austin Walkup
 * Aug 29, 2011
 *
 * GenericLexer header file
 * 		GenericLexer is a full lexer however is does not contain a grammer
 * 		therefore you cannot create a GenericLexer you must inherit from it
 * 		and define the grammer using the inherited method and create the scan method:
 * 		'reserve(std::string, Token)'
*/

#ifndef __GENERIC_LEXER__WALKUP__
#define __GENERIC_LEXER__WALKUP__

#include <map>//for mapping keywords to Tokens
#include <string>
#include <sstream>//for next_word
#include <fstream>//for input file
#include <ctype.h>//for isspace()
#include "Token.h"//for enums

class GenericLexer {
	public:
		GenericLexer(std::string);
		GenericLexer(const GenericLexer&);
		~GenericLexer();
		virtual Token getToken() = 0;
		
	private:
		std::map<std::string, Token::Tag> lex_map;

	protected:
		int line;
		int pos;
		void reserve(std::string, Token::Tag);
		Token::Tag lookup(const std::string&);
		std::string next_word();
		std::ifstream in;
};


#endif /* __GENERIC_LEXER__WALKUP__ */
