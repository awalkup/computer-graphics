/*
 * Austin Walkup
 * Sep 11, 2011
 *
 * Matrix header file
 * 		Matrix class for transformations on data
*/

#ifndef __MATRIX_H__WALKUP__
#define __MATRIX_H__WALKUP__

#include <iostream>
#include <algorithm>
#include <cmath>
#include "Vector.h"

class Matrix {
	public:
		//arbatrary matrix
		Matrix( float, float, float, float,
				float, float, float, float,
				float, float, float, float,
				float, float, float, float);
		Matrix();
		Matrix(const Matrix&);
		~Matrix();
		Matrix& operator=(const Matrix&);

		int get_size();
		Matrix operator*(Matrix&);
		void operator*(float*);
		Matrix& transpose();
		friend std::ostream& operator<<(std::ostream&, const Matrix&);
	private:
		void create_matrix( float, float, float, float,
							float, float, float, float,
							float, float, float, float,
							float, float, float, float);
		int size;
		float matrix[4][4];
};
std::ostream& operator<<(std::ostream&, const Matrix&);

Matrix create_rotate(float, float, float, float);
Matrix create_scale(float, float, float);
Matrix create_scale(float);
Matrix create_translate(float, float, float);

#endif /* __MATRIX_H__WALKUP__ */
