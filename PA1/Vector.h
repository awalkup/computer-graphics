/*
 * Austin Walkup
 * Sep 11, 2011
 *
 * Vector header file
 * 		Vector class for vector operations
*/

#ifndef __VECTOR_H__WALKUP__
#define __VECTOR_H__WALKUP__

#include <cmath>
#include <vector>

class Vector {
	public:
		Vector();
		Vector(float, float, float);
		Vector(float*, float*, int size = 4);
		Vector(const Vector&);
		Vector& operator=(Vector);
		float magnatude();
		Vector& operator*(float);
		Vector& operator*(int);
		float operator*(Vector&);
		float& operator[](int);
		Vector cross(Vector&);
		Vector& normalize();
		
		private:
			std::vector<float> vec;
			float mag;
};

Vector& operator*(float, Vector&);
Vector& operator*(int, Vector&);
Vector create_normal(Vector&, Vector&);

#endif /* __VECTOR_H__WALKUP__ */
