/*
 * Austin Walkup
 * Sep 11, 2011
 *
 * Matrix class file
 * 		Matrix class for 4x4 transformation matrices on data
*/

#include "Matrix.h"

/*
 * Constructor
 * @param x[1-4] - x coordinate for [1-4] row
 * @param y[1-4] - y coordinate for [1-4] row
 * @param z[1-4] - z coordinate for [1-4] row
 * @param w[1-4] - homogeneous coordinate for [1-4] row
*/
Matrix::Matrix( float x1, float y1, float z1, float w1,
				float x2, float y2, float z2, float w2,
				float x3, float y3, float z3, float w3,
				float x4, float y4, float z4, float w4){

	//matrix = new float[4][4];
	create_matrix(x1,y1,z1,w1,
				  x2,y2,z2,w2,
				  x3,y3,z3,w3,
				  x4,y4,z4,w4);
}//Matrix

/*
 * Constructor
*/
Matrix::Matrix(){
	//matrix = new float[4][4];
	create_matrix(  0,0,0,0,
					0,0,0,0,
					0,0,0,0,
					0,0,0,0);
}//Matrix

/*
 * Copy Constructor
*/
Matrix::Matrix(const Matrix& rhs){
	size = rhs.size;
	for(int i=0; i<rhs.size; i++){
		for(int j=0; j<rhs.size; j++){
			matrix[i][j] = rhs.matrix[i][j];
		}
	}
}

/*
 * Destructor
*/
Matrix::~Matrix(){
	//delete[] *matrix;
	//delete[] matrix;
}

/*
 * Opertator= Assigns the values of 'rhs' to this matrix;
 * @param rhs - Matrix to set equal to
 * @ret - Matrix that is has had it's values copied
*/
Matrix& Matrix::operator=(const Matrix& rhs){
	size = rhs.size;
	for(int i=0; i<rhs.size; i++){
		for(int j=0; j<rhs.size; j++){
			matrix[i][j] = rhs.matrix[i][j];
		}
	}
	return *this;
}

/*
 * Fills the values for a 4x4 matrix
*/
void Matrix::create_matrix( float x1, float y1, float z1, float w1,
							float x2, float y2, float z2, float w2,
							float x3, float y3, float z3, float w3,
							float x4, float y4, float z4, float w4){
	matrix[0][0] = x1;
	matrix[0][1] = y1;
	matrix[0][2] = z1;
	matrix[0][3] = w1;

	matrix[1][0] = x2;
	matrix[1][1] = y2;
	matrix[1][2] = z2;
	matrix[1][3] = w2;

	matrix[2][0] = x3;
	matrix[2][1] = y3;
	matrix[2][2] = z3;
	matrix[2][3] = w3;

	matrix[3][0] = x4;
	matrix[3][1] = y4;
	matrix[3][2] = z4;
	matrix[3][3] = w4;

	size = 4;
}

int Matrix::get_size(){
	return size;
}

/*
 * Operator Multiply
 * @param rhs - Matrix to multiply with
 * @ret - New matrix after the multiplication
*/
Matrix Matrix::operator*(Matrix& rhs){
	/* 
	 * Optimizations found at:
	 * http://blogs.msdn.com/b/xiangfan/archive/2009/04/28/optimize-your-code-matrix-multiplication.aspx
	 */
	Matrix result;
	
	rhs.transpose();//to reduce cache missing

	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++){
			float c = 0;//to reduce lost of mem writes
			for(int k=0; k<size; k++){
				c += matrix[i][k] * rhs.matrix[j][k];
			}//for k
			result.matrix[i][j] = c;
		}//for j
	}//for i
	rhs.transpose();

	return result;
}//operator *

void Matrix::operator*(float* vector){
	float x = vector[0];
	float y = vector[1];
	float z = vector[2];
	float w = vector[3];
	for(int i=0; i<size; i++){
		vector[i] = matrix[i][0]*x + matrix[i][1]*y +
			matrix[i][2]*z + matrix[i][3]*w;
	}
}

/*
 * Transposes the matrix. Swaps the matrix's rows and col in place
 * @ret - Referenece to this matrix
*/
Matrix& Matrix::transpose(){
	for(int i=0; i<size; i++){
		for(int j=i+1; j<size;j++){
			std::swap(matrix[i][j], matrix[j][i]);
		}//for j
	}//for i

	return *this;
}//transpose

/*
 * Creates a rotation matrix given the amount to rotate and
 * the axis to rotate about
 * @param radian - The amount to rotate in radians
 * @param x - The x coordinate for the axis of rotation
 * @param y - The y coordinate for the axis of rotation
 * @param z - The z coordiante for the axis of rotation
 * @ret - Matrix that will rotate by 'radian' radians about the <x,y,z> axis
*/
Matrix create_rotate(float radian, float x, float y, float z){
	Vector w(x,y,z);
	w.normalize();//normalize w

	Vector u = w;//create copy of w

	//find the smallest value of u (aka w) and set to 1
	float& min = u[0];
	if(min > u[1])
		min = u[1];
	if(min > u[2])
		min = u[2];
	min = 1;

	u.normalize();//re-normalize

	Vector v = w.cross(u);//create orthogonal vector v
	v.normalize();//normalize v since there is no guarantee it is unit

	Vector m = w.cross(v);//create orthonormal vector m

	Matrix r_w( v[0], v[1], v[2], 0,
				m[0], m[1], m[2], 0,
				w[0], w[1], w[2], 0,
				  0,    0,    0,  1 );

	Matrix rotate(  cos(radian), -sin(radian), 0, 0,
					sin(radian),  cos(radian), 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1 );
	Matrix temp = rotate * r_w;
	return r_w.transpose() * temp;
}

/*
 * Creates a scaling matrix given the amount to scale
 * @param sx - The amount to scale the x coord by
 * @param sy - The amount to scale the y coord by
 * @param sz - The amount to scale the z coord by
 * @ret - Matrix that will scale x by sx, y by sy, and z by sz
*/
Matrix create_scale(float sx, float sy, float sz){
	return Matrix(  sx,0,0,0,
					0,sy,0,0,
					0,0,sz,0,
					0,0,0, 1 );
}

/*
 * Creates a scaling matrix given the amount to scale
 * @param uniform_scaler - The amount to scale all the data by
 * @ret - Matrix that will scale the x, y, and z coords by 'uniform_scaler'
*/
Matrix create_scale(float uniform_scaler){
	return create_scale(uniform_scaler, uniform_scaler, uniform_scaler);
}

/*
 * Creates a translation matrix in homogeneous coords
 * @param tx - The amount to translate x by
 * @param ty - The amount to translate y by
 * @param tz - The amount to translate z by
 * @ret - Matrix that will translate x by tx, y by ty, and z by tz
*/
Matrix create_translate(float tx, float ty, float tz){
	return Matrix(  1,0,0,tx,
					0,1,0,ty,
					0,0,1,tz,
					0,0,0,1  );
}


std::ostream& operator<<(std::ostream& out, const Matrix& mat){
	out.precision(3);
	out << mat.matrix[0][0] <<'\t'<< mat.matrix[0][1] <<'\t';
	out << mat.matrix[0][2] <<'\t'<< mat.matrix[0][3] <<'\n';
	out << mat.matrix[1][0] <<'\t'<< mat.matrix[1][1] <<'\t';
	out << mat.matrix[1][2] <<'\t'<< mat.matrix[1][3] <<'\n';
	out << mat.matrix[2][0] <<'\t'<< mat.matrix[2][1] <<'\t';
	out << mat.matrix[2][2] <<'\t'<< mat.matrix[2][3] <<'\n';
	out << mat.matrix[3][0] <<'\t'<< mat.matrix[3][1] <<'\t';
	out << mat.matrix[3][2] <<'\t'<< mat.matrix[3][3] <<'\n';
	return out;
}
