/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * CmdParser header file
 * 		Parser that has methods to parse a command file 
 *
*/

#ifndef __CMD_PARSER_H__WALKUP__
#define __CMD_PARSER_H__WALKUP__

#include <string>
#include <sstream>
#include <map>
#include <algorithm>
#include "Token.h"
#include "CmdLexer.h"
#include "Matrix.h"

class CmdParser {
	public:
		CmdParser(CmdLexer&);
		//default destructor fine
		std::map<std::string, Matrix> parse_cmd();

	private:
		void unexpectedToken(Token, std::string);
		void generic_error(Token, std::string);
		void create_rmatrix(std::map<std::string, Matrix>&);
		void create_smatrix(std::map<std::string, Matrix>&);
		void create_tmatrix(std::map<std::string, Matrix>&);
		void create_amatrix(std::map<std::string, Matrix>&);
		CmdLexer &lex;
};

#endif /* __CMD_PARSER_H__WALKUP__ */
