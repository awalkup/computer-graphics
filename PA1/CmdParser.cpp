/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * CmdParser class file
 * 		Parser that has methods to parse a command file 
 *
*/

#include "CmdParser.h"
using std::string;
using std::stringstream;
using std::map;


CmdParser::CmdParser(CmdLexer &lexer): lex(lexer) {}

//default copy constructor fine

//default destructor fine

/*
 * Parses a .cmd file and returns a map from names of groups to a Matrix.
 * std::string exceptions are thrown if errors are encountered.
 * @ret - a map from std::string names of Groups to a transformation Matrix
*/
map<string, Matrix> CmdParser::parse_cmd(){
	map<string, Matrix> matrix_map;

	Token next = lex.getToken();
	for(;;){
		if(next == Token::END_OF_FILE){
			break;
		}//if EOF

		else if(next == Token::ROTATE){
			create_rmatrix(matrix_map);
		}//else if rotate

		else if(next == Token::TRANSLATE){
			create_tmatrix(matrix_map);
		}//else if translate

		else if(next == Token::SCALE){
			create_smatrix(matrix_map);
		}//else if scale

		else if(next == Token::ARBITRARY){
			create_amatrix(matrix_map);
		}//else if arbitrary

		else if(next != Token::NEWLINE){
			unexpectedToken(next, "ROTATE, TRANSLATE, SCALE, ARBITRARY, EOF");
		}//else if not newline
		next = lex.getToken();
	}//for(;;)
	return matrix_map;
}

/*
 * Throws a std::string exception for Unexpected Tokens
 * @param tok - The unexpected Token
 * @param expected - A string of Token names that you expected to recieve
*/
void CmdParser::unexpectedToken(Token tok, string expected){
	stringstream e;
	e<<"ERROR: Unexpected Token "<<tok.line<<":"<<tok.pos;
	e<<" Got "<<(string)tok;
	e<<" Expected {" <<expected<<"}.\n";
	throw e.str();
}

/*
 * Throws a std::string exception for any generic error
 * @param tok - Token that caused the error
 * @param error - The specific error message that is to be thrown
*/
void CmdParser::generic_error(Token tok, string error){
	stringstream e;
	e<<"ERROR: "<<tok.line<<":"<<tok.pos;
	e<<error<<'\n';
	throw e.str();
}


/*
 * Parses syntax for a rotation matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_rmatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	conv << next.get_data();
	float radian = 0;
	conv >> radian;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float x = 0;
	conv << next.get_data();
	conv >> x;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float y = 0;
	conv << next.get_data();
	conv >> y;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float z = 0;
	conv << next.get_data();
	conv >> z;
	conv.str("");
	conv.clear();

	Matrix r = create_rotate(radian, x, y, z);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = r * matrix_map[name];
	}
	else{
		matrix_map[name] = r;
	}

}//create_rmatrix

/*
 * Parses syntax for a scaling matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_smatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float x = 0;
	conv << next.get_data();
	conv >> x;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float y = 0;
	conv << next.get_data();
	conv >> y;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float z = 0;
	conv << next.get_data();
	conv >> z;
	conv.str("");
	conv.clear();

	Matrix s = create_scale(x, y, z);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = s * matrix_map[name];
	}
	else{
		matrix_map[name] = s;
	}

}//create_smatrix

/*
 * Parses syntax for a translation matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_tmatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float x = 0;
	conv << next.get_data();
	conv >> x;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float y = 0;
	conv << next.get_data();
	conv >> y;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	float z = 0;
	conv << next.get_data();
	conv >> z;
	conv.str("");
	conv.clear();

	Matrix t = create_translate(x, y, z);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = t * matrix_map[name];
	}
	else{
		matrix_map[name] = t;
	}

}//create_tmatrix

/*
 * Parses syntax for a arbitrary matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_amatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	float v[16];
	for(int i=0; i<16; i++){
		next = lex.getToken();
		if(next != Token::FLOAT && next != Token::INTEGER){
			unexpectedToken(next, "FLOAT, INTEGER");
		}
		conv << next.get_data();
		conv >> v[i];
		conv.str("");
		conv.clear();
	}

	Matrix a(v[0], v[1], v[2], v[3],
			v[4], v[5], v[6], v[7],
			v[8], v[9], v[10], v[11],
			v[12], v[13], v[14], v[15]);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = a * matrix_map[name];
	}
	else{
		matrix_map[name] = a;
	}

}//create_amatrix
