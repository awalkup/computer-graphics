/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * VertexList class file
 * 		VertexList class that contains a list of vertices, and maintains their order
*/

#include "VertexList.h"

using std::vector;
using std::string;
using std::stringstream;

VertexList::~VertexList(){
	vector<float*>::iterator it = vertex_list.begin();
	for(; it != vertex_list.end(); ++it){
		delete[] *it;
	}
}

/*
 * Addes a vertex to the end of the list and returns the position it was added at
 * @param x - The x-coordinate
 * @param y - The y-coordinate
 * @param z - The z-coordinate
 * @param w - The homogeneous coordinate (default 1.0)
 * @ret - Unsigned integer where the vertex was added in the list
*/
unsigned VertexList::add_vertex(float x, float y, float z, float w) {
	float *vertex = new float[4];
	vertex[0] = x;
	vertex[1] = y;
	vertex[2] = z;
	vertex[3] = w;
	vertex_list.push_back(vertex);
	return vertex_list.size();
}

/*
 * Returns a float* of four floats at the specified index, does not check bounds
 * @param index - The position where the float* is located
 * @ret - A float* of four floats (x,y,z,w)
*/
float* VertexList::operator[](unsigned index) {
	return vertex_list[index-1];
}

/*
 * Changes the value of the vertex at index to the float* [x,y,z,w]
 * @param index - Position of the vertex that is to be changed 1 based
 * @param x - New x-coordinate
 * @param y - New y-coordinate
 * @param z - New z-coordinate
 * @param w - New homogeneous coordinate (default 1.0)
 * @ret - Boolean that is true if the reassignment worked, false otherwise
*/
bool VertexList::reassign(unsigned index, float x, float y, float z, float w) {
	if(index >= vertex_list.size())
		return false;

	float* vertex = vertex_list[index-1];
	vertex[0] = x;
	vertex[1] = y;
	vertex[2] = z;
	vertex[3] = w;

	vertex_list[index] = vertex;
	return true;
}

/*
 * Outputs all the vertices in OBJ format
 * @ret - A string that is all the vertices in OBJ format
*/
string VertexList::to_obj(){
	stringstream ss;
	
	vector<float*>::iterator it = vertex_list.begin();
	for(; it != vertex_list.end(); ++it){
		ss << "v " <<(*it)[0]<< " " <<(*it)[1]<< " ";
		ss <<(*it)[2];
		if((*it)[3] != 1)
			ss<<" "<<(*it)[3];
		ss<<'\n';
	}
	return ss.str();
}

/*
 * Method to round to the nearest specified decimal place
 * @param val - The  value to round
 * @ret - The value rounded to the nearest specified decimal place
*/
#define DECIMAL_PLACE 10000000
float VertexList::round(float val){
	return float(int(val * DECIMAL_PLACE + 0.5)) / DECIMAL_PLACE;
}
