/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * VertexList header file
 * 		VertexList class that contains a list of vertices, and maintains their order
*/

#ifndef __VERTEXLIST_H__WALKUP__
#define __VERTEXLIST_H__WALKUP__

#include <vector>
#include <string>
#include <sstream>

class VertexList {
	public:
		~VertexList();
		unsigned add_vertex(float x, float y, float z, float w = 1.0);
		float* operator[](unsigned);
		bool reassign(unsigned index, float x, float y, float z, float w = 1.0);
		std::string to_obj();

	private:
		std::vector<float*> vertex_list;
		float round(float);
};


#endif /* __VERTEXLIST_H__WALKUP__ */
