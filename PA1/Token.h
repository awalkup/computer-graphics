/* 
 * Austin Walkup
 * Aug 30, 2011
 *
 * Token header file
 * 		Class for defining the grammar for Generic Lexer
 *
*/

#ifndef __TOKEN_H__WALKUP__
#define __TOKEN_H__WALKUP__

#include <string>

class Token {
	public:
		enum Tag{
			VERTEX,
			FACE,
			GROUP,
			COMMENT,
			ID,
			FLOAT,
			NEWLINE,
			INTEGER,
			ROTATE,
			TRANSLATE,
			SCALE,
			ARBITRARY,
			END_OF_FILE,
			NONE
		};

		Token();
		Token(int, int, Tag, std::string v = "");
		operator std::string();
		bool operator==(Tag);
		bool operator==(Token);
		bool operator!=(Tag);
		bool operator!=(Token);
		std::string get_data();
		Tag get_tag();

		int line;
		int pos;

	private:		
		Tag tag;
		std::string value;
};


#endif /* TOKEN_H__WALKUP__*/
