/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Face class file
 * 		Face class that contains a list of vertices
*/

#include "Face.h"

using std::string;
using std::vector;
using std::stringstream;

/*
 * Constructor
*/
Face::Face(VertexList& v){
	v_list = &v;
}

/*
 * Addes the index of a vertex to the face's vertex list
 * @param index - The index of the vertex in some static list of vertices
*/
void Face::add_vertex(unsigned index){
	index_list.push_back(index);
}

/*
 * Checks to see if the face is valid
 * Throws a std::string exception if the face is not vaild
*/
void Face::check_face(){
	if(index_list.size() <3){
		stringstream e;
		e << "An error has occured. Face f ";
		e << index_list[0] << " " << index_list[1];
		e << " Only contains two vertices. Three are required.\n";
		throw e.str();
	}
	
	unsigned offset = 1;
	while(normal.magnatude() == 0){
		if(index_list.size() <= offset+1){
			stringstream e;
			e << "An error has occured. Face f ";
			for(unsigned i=0; i<index_list.size();i++)
				e << index_list[i] << " ";
			e << " Has all vertices that are colinear.\n";
			throw e.str();	
		}
		//creates the surface normal vector with 
		//point 0 and offset, and offset and offset+1
		Vector ab( (*v_list)[index_list[0]], (*v_list)[index_list[offset]] );
		Vector bc( (*v_list)[index_list[offset]], (*v_list)[index_list[offset+1]] );
		normal = create_normal(ab, bc);
		offset++;
	}
	
	normal = normal.normalize();

	for(unsigned i = offset; i < index_list.size(); i++){
		Vector next( (*v_list)[index_list[i-1]], (*v_list)[index_list[i]] );
		float dot = normal*next;
		if(dot >= 0.01 || dot <= -0.01){
			stringstream e;
			e << "An error has occured. Face f ";
			for(unsigned i=0; i<index_list.size();i++)
				e << index_list[i] << " ";
			e << " Is not coplaner.\n";
			throw e.str();	
		
		}
	}
}

/*
 * Creates a string of the face's data in .obj format
 * @ret - A std::string in .obj format for the Face
*/
string Face::to_obj(){
	stringstream ss;
	ss<<"f ";
	
	vector<unsigned>::iterator it = index_list.begin();
	for(; it != index_list.end(); ++it){
		ss<<*it<<" ";
	}
	return ss.str();
}

/*
 * Returns a list of indices for the vertices in the Face
 * @ret - A vector of unsigned integers that are indices into a static list of vertices
*/
vector<unsigned>& Face::get_vertices(){
	return index_list;
}
