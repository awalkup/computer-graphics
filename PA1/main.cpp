
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include "Parser.h"
#include "CmdParser.h"
#include "Lexer.h"
#include "CmdLexer.h"
#include "Group.h"
#include "Face.h"
#include "VertexList.h"
#include "Matrix.h"

using namespace std;

int main(int argc, char **argv){
	try{
		if(argc < 4){
			cout<<"Usage:\n\t"<<argv[0];
			cout<<" <input.obj> <transform.cmd> <output.obj>\n";
			return -1;
		}
		VertexList v_list;

		Lexer myLex(argv[1]);
		CmdLexer cmdLex(argv[2]);
		ofstream out(argv[3]);
		if(!out){
			cerr<<"Could not open file "<<argv[3]<<'\n';
		}

		Parser parser(myLex);
		CmdParser cmdparser(cmdLex);

		map<string, Group> groups = parser.parse_obj(v_list);;
		map<string, Matrix> matrices = cmdparser.parse_cmd();;

		map<string, Matrix>::iterator mats = matrices.begin();
		for(;mats != matrices.end(); ++mats){
			if(groups.find(mats->first) == groups.end())
				throw "Group "+mats->first+" does not exist";
			set<unsigned> vert_set = groups.find(mats->first)->second.get_vertices();
			set<unsigned>::iterator it = vert_set.begin();
			for(; it != vert_set.end(); ++it){
				mats->second * v_list[*it];
			}
		}

		//print to file
		map<string, Group>::iterator it = groups.begin();

		out<<v_list.to_obj()<<'\n';
		for(; it!= groups.end(); ++it){
			out<<it->second.to_obj()<<'\n';
		}

	}catch(std::string e ){
		cerr<<e<<endl;
		return -1;
	}

	return 0;
}
