/*
 * Austin Walkup
 * Sep 11, 2011
 *
 * Vector class file
 * 		Vector class for vector operations
*/

#include "Vector.h"

/*
 * Constructor
*/
Vector::Vector(){
	mag = 0;
	vec.push_back(0);
	vec.push_back(0);
	vec.push_back(0);
	vec.push_back(1);
}

/*
 * Constructor
 * @param x - x coordinate of the vector
 * @param y - y coordinate of the vector
 * @param z - z coordinate of the vector
*/
Vector::Vector(double x, double y, double z, double w){
	mag = sqrt( (x*x) + (y*y) + (z*z) );
	vec.push_back(x);
	vec.push_back(y);
	vec.push_back(z);
	vec.push_back(w);
}

/*
 * Constructor
 * @param a - The first point
 * @param b - The second point
 * @param size - The size of the point (i.e 4 for 3D point with homogeneous coords)
*/
Vector::Vector(double* a, double* b, int size){
	double sum_of_sqs = 0;
	//size -1 because we don't want the homo coord
	for(int i = 0; i<size-1; i++){
		vec.push_back( b[i] - a[i]);
		sum_of_sqs += (vec[i]*vec[i]);
	}
	vec.push_back(1); //Make the homogeneous coord 1
	mag = sqrt(sum_of_sqs);
}

Vector::Vector(Vector v1, Vector v2){
	double sum_of_sqs = 0;
	//size -1 because we don't want the homo coord
	for(unsigned i = 0; i<v1.size()-1; i++){
		vec.push_back( v2[i] - v1[i]);
		sum_of_sqs += (vec[i]*vec[i]);
	}
	vec.push_back(1); //Make the homogeneous coord 1
	mag = sqrt(sum_of_sqs);
}

/*
 * Copy Constructor
*/
Vector::Vector(const Vector& rhs){
	vec = rhs.vec;
	mag = rhs.mag;
}

Vector& Vector::operator+=(const Vector& rhs){
	return *this = *this + rhs;
}

/*
 * Addes one vector to another
 * @param rhs - The second vector to add
 * @ret - A new Vector the is the result of summing the two vectors
*/
Vector Vector::operator+(const Vector& rhs){
	Vector new_vec = rhs;
	double sum_of_sqrs = 0;
	for(unsigned i=0; i< vec.size()-1; i++){
		new_vec[i] += vec[i];
		sum_of_sqrs += (new_vec[i]*new_vec[i]);
	}
	new_vec.mag = sqrt(sum_of_sqrs);
	return new_vec;
}

/*
 * Subtracts one vector from another
 * @param rhs - The second vector to subtract
 * @ret - A new Vector that is the result of the subtraction
*/
Vector Vector::operator-(const Vector& rhs){
	Vector new_vec = *this;
	double sum_of_sqrs = 0;
	for(unsigned i=0; i< vec.size()-1; i++){
		new_vec[i] -= rhs.vec[i];
		sum_of_sqrs += (new_vec[i]*new_vec[i]);
	}
	new_vec.mag = sqrt(sum_of_sqrs);
	return new_vec;
}

/*
 * Operator = assins one vector to antoher's values
 * @param rhs - Vector to copy
*/
Vector& Vector::operator=(Vector rhs){
	vec = rhs.vec;
	mag = rhs.mag;
	return *this;
}

/*
 * Gets the magnatude of the Vector
 * @ret - The magnatude of the Vector
*/
double Vector::magnatude(){
	return mag;
}

/*
 * Gets the value of the vector at index
 * @param index - The index to get the value of
 * @ret - Value of the vector at index
*/
double& Vector::operator[](int index){
	return vec[index];
}

/*
 * Operator * multiply the vector by a scaler
 * @param scaler - The value to multiply the vector by
 * @ret - Refrence to the vector that was changed
*/
Vector& Vector::operator*(double scaler){
	if(vec[3] != 1){
		this->normalize();
	}

	//all except the homogeneous coord
	double sum_of_sqrs = 0;
	for(unsigned i=0; i<vec.size()-1; i++){
		vec[i] *= scaler;
		sum_of_sqrs += (vec[i]*vec[i]);
	}
	mag = sqrt(sum_of_sqrs);
	return *this;
}

/*
 * Operator * multiply the vector by a scaler
 * @param scaler - The value to multiply the vector by
 * @ret - Refrence to the vector that was changed
*/
Vector& Vector::operator*(int scaler){
	return (*this * (double)scaler);
}

/*
 * Multiply the vector by another vector NOTE: this is the dot product
 * @param rhs - Vector to multiply with
 * @ret - A scaler that is the dot product of the Vectors
*/
double Vector::operator*(Vector& rhs){
	if(vec[3] != 1){
		this->normalize();
	}
	if(rhs.vec[3] != 1){
		rhs.normalize();
	}

	double sum = 0;
	//all but homogeneous coords
	for(unsigned i=0; i<vec.size() -1; i++){
		sum += vec[i] * rhs.vec[i];
	}
	return sum;
}

/*
 * Cross product of the Vectors. 
 * NOTE: This only works with Vectors of size 4 in homogeneous coords
 * @param rhs - Vector to take the cross product with
 * @ret - A vector that is orthagonal to the input Vectors
*/
Vector Vector::cross(Vector& rhs){
	Vector orthoVec;
	if(vec.size() != 4){
		return orthoVec; //only works for 3x3 with homo coords
	}
	orthoVec[0] = ((vec[1]*rhs.vec[2]) - (vec[2]*rhs.vec[1]));//x-coord
	orthoVec[1] = ((vec[2]*rhs.vec[0]) - (vec[0]*rhs.vec[2]));//y-coord
	orthoVec[2] = ((vec[0]*rhs.vec[1]) - (vec[1]*rhs.vec[0]));//z-coord
	orthoVec[3] = 1;//homogeneous coord
	
	double sum_of_sqs = 0;
	for(unsigned i=0; i< vec.size() -1; i++){
		sum_of_sqs += (orthoVec.vec[i]* orthoVec.vec[i]);
	}
	orthoVec.mag = sqrt(sum_of_sqs);
	return orthoVec;
}

/*
 * Normalizes the vector
 * @ret - The same vector but normalized
*/
Vector& Vector::normalize(){
	double a = 0;
	for(unsigned i=0; i<vec.size()-1; i++)
		a += (vec[i] * vec[i]);
	double b = sqrt(a);
	if(mag == 0)
		return *this;
	for(unsigned i=0; i<vec.size()-1; i++){
		vec[i] /= b;
	}
	vec[vec.size()-1] = 1;//homo coord
	mag = 1;
	return *this;
}

/*
 * Multiply a scaler by a Vector
 * @param Scaler - scaler to multiply by
 * @param rhs - The vector to scale
 * @ret - The Vector that was scaled
*/
Vector& operator*(double scaler, Vector &rhs){
	return rhs * scaler;
}

/*
 * Multiply a scaler by a Vector
 * @param Scaler - scaler to multiply by
 * @param rhs - The vector to scale
 * @ret - The Vector that was scaled
*/
Vector& operator*(int scaler, Vector &rhs){
	return rhs * scaler;
}

/*
 * Creates a Normal vector given two vectors
 * @param ab - The first Vector
 * @param bc - The second Vector
 * @ret - A normal Vector specified by the given Vectors
*/
Vector create_normal(Vector &ab, Vector &bc){
	return ab.cross(bc);
}
