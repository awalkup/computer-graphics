/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Group header file
 * 		Group class that contains a list of faces
*/

#ifndef __GROUP_H__WALKUP__
#define __GROUP_H__WALKUP__

#include "Face.h"
#include <algorithm>
#include <vector>
#include <set>
#include <string>
#include <sstream>//for to_obj

class Group {
	public:
		Group(std::string name = "default");

		/* I want the Face to be copied so that I can added 
		   it inside a parser function */
		Group(std::string, Face, Face, Face); 

		Group(std::string, std::vector<Face>&);

		//again I want it to be copied
		void addFace(Face);

		//Verifies that the faces in the group don't reference a
		//vertex from another group, that the polygon formed
		//is convex and that faces aren't significantly non-coplaner
		void check_group();
		
		void add_vertex(unsigned);
		std::string to_obj();
		std::string get_name();
		std::set<unsigned>& get_vertices();
		std::vector<Face>& get_faces() { return face_list; }
		void set_name(std::string);
				
	private:
		std::string name;
		std::set<unsigned> grp_vertices;
		std::vector<Face> face_list;
};


#endif /* __GROUP_H__WALKUP__ */
