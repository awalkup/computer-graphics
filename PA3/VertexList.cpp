/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * VertexList class file
 * 		VertexList class that contains a list of vertices, and maintains their order
*/

#include "VertexList.h"

using std::vector;
using std::string;
using std::stringstream;

/*
 * Constructor
*/
VertexList::VertexList() {}

/*
 * Copy Constructor
*/
VertexList::VertexList(const VertexList &rhs){
	for(unsigned i=0; i<rhs.vertex_list.size(); i++){
		add_vertex(rhs.vertex_list[i][0], rhs.vertex_list[i][1], 
					rhs.vertex_list[i][2],rhs.vertex_list[i][3]);
	}
}

/*
 * Destructor
*/
VertexList::~VertexList(){
	vector<double*>::iterator it = vertex_list.begin();
	for(; it != vertex_list.end(); ++it){
		delete[] *it;
	}
}

/*
 * Addes a vertex to the end of the list and returns the position it was added at
 * @param x - The x-coordinate
 * @param y - The y-coordinate
 * @param z - The z-coordinate
 * @param w - The homogeneous coordinate (default 1.0)
 * @ret - Unsigned integer where the vertex was added in the list
*/
unsigned VertexList::add_vertex(double x, double y, double z, double w) {
	double *vertex = new double[4];
	vertex[0] = x;
	vertex[1] = y;
	vertex[2] = z;
	vertex[3] = w;
	vertex_list.push_back(vertex);
	return vertex_list.size();
}

/*
 * Returns a double* of four doubles at the specified index, does not check bounds
 * @param index - The position where the double* is located
 * @ret - A double* of four doubles (x,y,z,w)
*/
double* VertexList::operator[](unsigned index) {
	return vertex_list[index-1];
}

/*
 * Changes the value of the vertex at index to the double* [x,y,z,w]
 * @param index - Position of the vertex that is to be changed 1 based
 * @param x - New x-coordinate
 * @param y - New y-coordinate
 * @param z - New z-coordinate
 * @param w - New homogeneous coordinate (default 1.0)
 * @ret - Boolean that is true if the reassignment worked, false otherwise
*/
bool VertexList::reassign(unsigned index, double x, double y, double z, double w) {
	if(index >= vertex_list.size())
		return false;

	double* vertex = vertex_list[index-1];
	vertex[0] = x;
	vertex[1] = y;
	vertex[2] = z;
	vertex[3] = w;

	vertex_list[index] = vertex;
	return true;
}

/*
 * Outputs all the vertices in OBJ format
 * @ret - A string that is all the vertices in OBJ format
*/
string VertexList::to_obj(){
	stringstream ss;
	
	vector<double*>::iterator it = vertex_list.begin();
	for(; it != vertex_list.end(); ++it){
		ss << "v " <<(*it)[0]<< " " <<(*it)[1]<< " ";
		ss <<(*it)[2];
		if((*it)[3] != 1)
			ss<<" "<<(*it)[3];
		ss<<'\n';
	}
	return ss.str();
}

/*
 * normalizes the vector with w
 * @param i - index of the list to normalize
*/
void VertexList::w_normalize(int i){
	i--;
	double w = vertex_list[i][3];
	vertex_list[i][0] /= w;
	vertex_list[i][1] /= w;
	vertex_list[i][2] /= w;
	vertex_list[i][3] /= w;
}
