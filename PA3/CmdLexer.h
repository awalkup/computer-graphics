/*
 * Austin Walkup
 * Sep 12, 2011
 *
 * CmdLexer header file
 * 		A full lexer to parse cmd files for CS410 for Fall 2011
*/

#ifndef __CMDLEXER__WALKUP__
#define __CMDLEXER__WALKUP__

#include "GenericLexer.h"
#include "Token.h"
#include <boost/regex.hpp>
#include <string>

class CmdLexer : public GenericLexer {
	public:
		CmdLexer(std::string);
		Token getToken();
};


#endif /* __CMDLEXER__WALKUP__ */
