/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Face header file
 * 		Face class that contains a list of vertices
*/

#ifndef __FACE_H__WALKUP__
#define __FACE_H__WALKUP__

#include "Vector.h"
#include "VertexList.h"
#include <string>
#include <vector>//for index list
#include <sstream>//for to_obj

class Face {
	public:
		//TODO: Uncomment if I change to triangles
		//Face(int, int, int);
		Face(VertexList&, std::string&);
		void add_vertex(unsigned);
		void check_face();
		std::string to_obj();
		std::vector<unsigned>& get_vertices();
		std::string& get_material(){ return material; }
		void compute_normal();
		Vector& get_normal(){ return normal; }

	private:
		/* TODO: Uncomment if I switch to triangles
		 * unsigned index1;
		 * unsigned index2;
		 * unsigned index3;
		*/
		std::vector<unsigned> index_list;
		VertexList *v_list;
		Vector normal;
		std::string material;
};

#endif /* __FACE_H__WALKUP__ */
