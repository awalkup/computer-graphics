/*
 * Austin Walkup
 * Oct 10, 2011
 *
 * ImgAlgorithm header file
 * 		All the algorithms for creating an image are here. Call draw
*/

#ifndef __IMG_ALGS_H__WALKUP__
#define __IMG_ALGS_H__WALKUP__

#include <vector>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <cfloat>
#include <cmath>
#include "VertexList.h"
#include "Vector.h"
#include "Matrix.h"
#include "Group.h"
#include "Face.h"
#include "Material.h"
#include "LightSource.h"

#define round(d) (d)>0 ? ((int)((d)+0.5)) : ((int)((d)-0.5))

typedef struct Point {
	Point(): x(0), y(0), color(-1, -1, -1), depth(0) {}
	Point(int x, int y, RGB color, double d): x(x), y(y), color(color.r, color.g, color.b), depth(d) {}
	bool operator<(const Point& rhs) const {
		if(y < rhs.y)
			return true;
		else if(y == rhs.y){
			if(x < rhs.x)
				return true;
		}
		return false;
	}
	int x;
	int y;
	RGB color;
	double depth;
} Point;

class ImgDrawer {
	public:
		ImgDrawer(double, double, double, double);
		std::vector<std::vector<RGB> > draw_wireFrame(Matrix&,std::map<std::string, Group>&, VertexList);
		std::vector<std::vector<RGB> > draw_surfaceRender(Matrix&,std::map<std::string, Group>&, VertexList, std::vector<LightSource>&, std::map<std::string, Material>&, Vector&);
	private:
		void set_pixel(int, int, RGB color = RGB(255, 255, 255));
		bool check_pixel(int, int, double);
		void bresenhams(int, int, int, int);
		void bresenhams(int, unsigned char, int, unsigned char, unsigned char*);
		void bresenhams(int, double, int, double, double*);
		bool clipping(int&, int&, int&, int&);
		int clipping_helper(int, int);
		void polygon_filling(std::multiset<Point>&);
		void scanLine_intersection(std::multiset<Point>&, double, double, double, double, Point&, Point&);
		RGB calculate_vertex_color(double*, Vector&, Material&, Vector&, std::vector<LightSource>&);
		void interpolate(int, int, const Point&, const Point&, unsigned char*, unsigned char*, unsigned char*, double*);
		
		double minx;
		double miny;
		double maxx;
		double maxy;
		std::vector<std::vector<RGB> > img;
		std::vector<std::vector<double> > z_buf;
		/*
		 * For clipping
		*/
		const static int INSIDE = 0; // 0000
		const static int LEFT = 1;   // 0001
		const static int RIGHT = 2;  // 0010
		const static int BOTTOM = 4; // 0100
		const static int TOP = 8;    // 1000
};

#endif /* __IMG_ALGS_H__WALKUP__ */
