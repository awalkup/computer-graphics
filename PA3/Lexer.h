/*
 * Austin Walkup
 * Aug 30, 2011
 *
 * CS410_Lexer header file
 * 		A full lexer for CS410 for Fall 2011
*/

#ifndef __LEXER__WALKUP__
#define __LEXER__WALKUP__

#include "GenericLexer.h"
#include "Token.h"
#include <boost/regex.hpp>
#include <string>

class Lexer : public GenericLexer {
	public:
		Lexer(std::string);
		Token getToken();
};


#endif /* __LEXER__WALKUP__ */
