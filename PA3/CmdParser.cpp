/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * CmdParser class file
 * 		Parser that has methods to parse a command file 
*/

#include "CmdParser.h"
using std::string;
using std::stringstream;
using std::map;
using std::vector;


CmdParser::CmdParser(CmdLexer &lexer): lex(lexer) {}

//default copy constructor fine

//default destructor fine

/*
 * Parses a .cmd file and returns a map from names of groups to a Matrix.
 * std::string exceptions are thrown if errors are encountered.
 * @ret - a map from std::string names of Groups to a transformation Matrix
*/
void CmdParser::parse_cmd(CmdData& data){
	map<string, Matrix> matrix_map;
	map<string, Camera>& camera_map = data.camera_map;
	map<string, Material>& material_map = data.material_map;
	vector<LightSource>& light_sources = data.light_sources;
	data.wireFrame_camera = "";
	data.surface_render = "";

	Token next = lex.getToken();
	for(;;){
		if(next == Token::END_OF_FILE){
			break;
		}//if EOF

		else if(next == Token::ROTATE){
			create_rmatrix(matrix_map);
		}//else if rotate

		else if(next == Token::TRANSLATE){
			create_tmatrix(matrix_map);
		}//else if translate

		else if(next == Token::SCALE){
			create_smatrix(matrix_map);
		}//else if scale

		else if(next == Token::ARBITRARY){
			create_amatrix(matrix_map);
		}//else if arbitrary

		else if (next == Token::CAMERA){
			create_camera(camera_map);
		}//else if camera

		else if (next == Token::WIRE_FRAME){
			data.wireFrame_camera = create_wireFrame(camera_map);
			break;
		}//else if wire frame

		else if (next == Token::MATERIAL){
			create_material(material_map);
		}//else if material

		else if (next == Token::LIGHT_SOURCE){
			create_lightSource(light_sources);
		}//else if light source

		else if (next == Token::SURFACE_RENDER){
			data.surface_render = create_surfaceRender(camera_map);
			break;
		}//else if surface render

		else if(next != Token::NEWLINE){
			unexpectedToken(next, "ROTATE, TRANSLATE, SCALE, ARBITRARY, CAMERA, WIRE_FRAME, MATERIAL, LIGHT_SOURCE, SURFACE_RENDER, EOF");
		}//else if not newline
		next = lex.getToken();
	}//for(;;)
	data.matrix_map = matrix_map;
}//parse_cmd

/*
 * Throws a std::string exception for Unexpected Tokens
 * @param tok - The unexpected Token
 * @param expected - A string of Token names that you expected to recieve
*/
void CmdParser::unexpectedToken(Token tok, string expected){
	stringstream e;
	e<<"ERROR: Unexpected Token "<<tok.line<<":"<<tok.pos;
	e<<" Got "<<(string)tok;
	e<<" Expected {" <<expected<<"}.\n";
	throw e.str();
}

/*
 * Throws a std::string exception for any generic error
 * @param tok - Token that caused the error
 * @param error - The specific error message that is to be thrown
*/
void CmdParser::generic_error(Token tok, string error){
	stringstream e;
	e<<"ERROR: "<<tok.line<<":"<<tok.pos;
	e<<error<<'\n';
	throw e.str();
}

/*
 * Parse the numbers that come from each command, values are stored in 'values'
 * @param values - An array of size 'size' where the numbers will be stored
 * @param size - The size of the array created, and number of numbers to parse
*/
void CmdParser::parse_numbers(double* values, int size){
	stringstream conv;
	for(int i=0; i<size; i++){
		Token next = lex.getToken();
		if(next != Token::FLOAT && next != Token::INTEGER){
			unexpectedToken(next, "FLOAT, INTEGER");
		}
		conv << next.get_data();
		conv >> values[i];
		conv.str("");
		conv.clear();
	}
}//parse_numbers


/*
 * Parses syntax for a rotation matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_rmatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[4];//rad, x, y, z
	parse_numbers(v, 4);

	Matrix r = create_rotate(v[0], v[1], v[2], v[3]);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = r * matrix_map[name];
	}
	else{
		matrix_map[name] = r;
	}

}//create_rmatrix

/*
 * Parses syntax for a scaling matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_smatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[3];
	parse_numbers(v, 3);

	Matrix s = create_scale(v[0], v[1], v[2]);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = s * matrix_map[name];
	}
	else{
		matrix_map[name] = s;
	}

}//create_smatrix

/*
 * Parses syntax for a translation matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_tmatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[3];
	parse_numbers(v, 3);

	Matrix t = create_translate(v[0], v[1], v[2]);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = t * matrix_map[name];
	}
	else{
		matrix_map[name] = t;
	}

}//create_tmatrix

/*
 * Parses syntax for a arbitrary matrix and adds it to the map
 * @param matrix_map - The map to add the matrix to
*/
void CmdParser::create_amatrix(map<string, Matrix>& matrix_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[16];
	parse_numbers(v, 16);

	Matrix a(v[0], v[1], v[2], v[3],
			v[4], v[5], v[6], v[7],
			v[8], v[9], v[10], v[11],
			v[12], v[13], v[14], v[15]);
	if(matrix_map.find(name) != matrix_map.end()){
		matrix_map[name] = a * matrix_map[name];
	}
	else{
		matrix_map[name] = a;
	}

}//create_amatrix

/*
 * create_camera - Parses and creates a camera object and adds it to the
 * list of cameras
 * @param camera_map - The list of cameras to add to
*/
void CmdParser::create_camera(map<string, Camera>& camera_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[10];
	parse_numbers(v, 10);

	camera_map[name] = Camera(name, v[0], v[1], v[2], v[3], 
								v[4], v[5], v[6], v[7], v[8], v[9]);

}//create_camera

/*
 * create_material - Creates a material object and adds it to the map
 * @param material_map - The map to add the material object to
*/
void CmdParser::create_material(map<string, Material>& material_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[5];
	parse_numbers(v, 5);

	material_map[name] = Material(name, v[0], v[1], v[2], v[3], v[4]);
}//create_material

/*
 * create_lightSource - create a point light source and adds it to the list
 * @param light_sources - The list to add the light source to
*/
void CmdParser::create_lightSource(vector<LightSource>& light_sources){
	double v[7];
	parse_numbers(v, 7);

	light_sources.push_back(LightSource(v[0], v[1], v[2], v[3], v[4], v[5], v[6]));
}

/*
 * create_wireFrame - Parses and addes wire frames image data to the
 * correct camera. Throws an std::string exception if the camera 
 * does not exist
 * @param camera_map - A list of all cameras (adds the data to the camera)
 * @ret - A std::string that is the name of the camera modified
*/
string CmdParser::create_wireFrame(map<string, Camera>& camera_map){
	Token next = lex.getToken();
	stringstream conv;

	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();

	double v[4];
	parse_numbers(v, 4);

	// Check if the camera exists
	if(camera_map.find(name) == camera_map.end()){
		// re-purpose conv to prevent creating a new stringstream
		conv << " Camera " << name <<" not defined in this scope.";
		generic_error(next, conv.str());
	}

	camera_map[name].imgPlane(v[0], v[1], v[2], v[3]);
	return name;
}//create_wireFrame

/*
 * create_surfaceRender - Parses and addes surface render image data to the
 * correct camera. Throws an std::string exception if the camera 
 * does not exist
 * @param camera_map - A list of all cameras (adds the data to the camera)
 * @ret - A std::string that is the name of the camera modified
*/
string CmdParser::create_surfaceRender(map<string, Camera>& camera_map){
	return create_wireFrame(camera_map);
}//create_surfaceRender
