/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * VertexList header file
 * 		VertexList class that contains a list of vertices, and maintains their order
*/

#ifndef __VERTEXLIST_H__WALKUP__
#define __VERTEXLIST_H__WALKUP__

#include <vector>
#include <string>
#include <sstream>

class VertexList {
	public:
		VertexList();
		VertexList(const VertexList&);
		~VertexList();
		unsigned add_vertex(double x, double y, double z, double w = 1.0);
		double* operator[](unsigned);
		bool reassign(unsigned index, double x, double y, double z, double w = 1.0);
		unsigned size() { return vertex_list.size(); }
		std::vector<double*>::iterator begin(){return vertex_list.begin();}
		std::vector<double*>::iterator end() { return vertex_list.end(); }
		std::string to_obj();
		void w_normalize(int);

	private:
		std::vector<double*> vertex_list;
};


#endif /* __VERTEXLIST_H__WALKUP__ */
