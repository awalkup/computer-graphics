/*
 * Austin Walkup
 * Oct 23, 2011
 *
 * Material class file 
 * 		Class that contains the material properties  for a Face
*/

#include "Material.h"

using std::string;
using std::stringstream;

/*
 * Default Constructor
*/
Material::Material(): name("default"), spec_reflect(0), 
						phong_reflect(1), lrm(create_scale(1,1,1)) {}

/*
 * Constructor - other than the name all other values are set to default
 * @param name - The name of the material
*/
Material::Material(string& name): name(name), spec_reflect(0),
						phong_reflect(1), lrm(create_scale(1,1,1)) {}

/*
 * Constructor
 * @param n - name of the Material
 * @param r - The red value
 * @param g - The green value
 * @param b - The blue value
 * @param s - The specular reflectance constant
 * @param a - The phong reflectance constant
*/
Material::Material(string n, double r, double g, double b, double s, double a): 
						name(n), spec_reflect(s), phong_reflect(a) {
	if ( r < 0 || g < 0 || b < 0){
		stringstream e;
		e << "ERROR: Material " << n 
		  << "'s lambertian reflectance matrix has negative values.";
		throw e.str();
	}
	else if( s < 0 ){
		stringstream e;
		e << "ERROR: Material " << n 
		  << "'s specular reflectance constant is negative.";
		throw e.str();
	}
	else if( r+s > 1 || g+s > 1 || b+s > 1 ){
		stringstream e;
		e << "ERROR: Material " << n 
		  << "'s r+s, g+s, or b+s value exceed 1.";
		throw e.str();	
	}
	else if( a < 1 || a > 200 ){
		stringstream e;
		e << "ERROR: Material " << n 
		  << "'s phong reflectance constant must be between 1 and 200.";
		throw e.str();	
	}
	create_LRM(r, g, b);
}

/*
 * Copy Constructor
*/
Material::Material(const Material& rhs){
	name = rhs.name;
	spec_reflect = rhs.spec_reflect;
	phong_reflect = rhs.phong_reflect;
	lrm = rhs.lrm;
}

/*
 * Assigns one material to another
 * @param rhs - The other material to copy
 * @ret - A reference to this Material
*/
Material& Material::operator=(const Material& rhs){
	name = rhs.name;
	spec_reflect = rhs.spec_reflect;
	phong_reflect = rhs.phong_reflect;
	lrm = rhs.lrm;
	return *this;
}

/*
 * Create the Lambertian reflectance matrix
 * @param r - The red value
 * @param g - The green value
 * @param b - The blue value
*/
void Material::create_LRM(double r, double g, double b){
	lrm = create_scale(r, g, b); //TODO: MAKE SURE THIS IS OK (4x4 instead of 3x3)!!
}
