/*
 * Austin Walkup
 * Sep 11, 2011
 *
 * Vector header file
 * 		Vector class for vector operations
*/

#ifndef __VECTOR_H__WALKUP__
#define __VECTOR_H__WALKUP__

#include <cmath>
#include <vector>

class Vector {
	public:
		Vector();
		Vector(double, double, double, double w = 1);
		Vector(double*, double*, int size = 4);
		Vector(Vector, Vector);
		Vector(const Vector&);
		Vector& operator=(Vector);
		double magnatude();
		Vector& operator+=(const Vector&);
		Vector operator+(const Vector&);
		Vector operator-(const Vector&);
		Vector& operator*(double);
		Vector& operator*(int);
		double operator*(Vector&);
		double& operator[](int);
		Vector cross(Vector&);
		Vector& normalize();
		unsigned size() { return vec.size(); }
		
		private:
			std::vector<double> vec;
			double mag;
};

Vector& operator*(double, Vector&);
Vector& operator*(int, Vector&);
Vector create_normal(Vector&, Vector&);

#endif /* __VECTOR_H__WALKUP__ */
