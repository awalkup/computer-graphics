/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * Parser class file
 * 		Parser that has methods to parse an obj file and a command file 
 *
*/

#include "Parser.h"
using std::string;
using std::stringstream;
using std::map;
using std::set;

Parser::Parser(Lexer &lexer): lex(lexer) {}


//default copy constructor fine

//default destructor fine

/* 
 * Parses a .obj file and returns a map from names to groups.
 * std::string exceptions are thrown if errors are encountered.
 * @param v_list - A VertexList that is a reference to a list where the vertices are to be stored
 * @ret - a map from std::string to Groups, where the key is the group's name
*/
map<string, Group> Parser::parse_obj(VertexList &v_list, map<string, Material>& material_map){
	map<string, Group> group_map;

	Token next = lex.getToken();
	Group curr_group;
	Material curr_material;
	for(;;){
		if(next == Token::END_OF_FILE){
			group_map[curr_group.get_name()] = curr_group;
			break;
		}//if EOF

		else if(next == Token::GROUP){
			group_map[curr_group.get_name()] = curr_group;
			curr_group = create_group(group_map);
		}//else if group

		else if(next == Token::VERTEX){
			create_vertex(v_list);
		}//else if vertex

		else if(next == Token::FACE){
			curr_group.addFace(create_face(v_list, curr_material));
		}//else if face

		else if (next == Token::USE_MATERIAL){
			material_map[curr_material.get_name()] = curr_material;
			curr_material = create_material(material_map);
		}

		else if(next != Token::NEWLINE){
			unexpectedToken(next, "GROUP, VERTEX, FACE, EOF");
		}//else if not newline
		next = lex.getToken();
	}//for(;;)
	check_integrity(group_map);
	return group_map;
}

/*
 * Checks the validity of the data in the file
 * Throws a std::string if an error has occured
*/
void Parser::check_integrity(map<string, Group>& grp_map){
	map<string, Group>::iterator it = grp_map.begin();
	//check if each individual group is correct
	for(; it != grp_map.end(); ++it){
		it->second.check_group();
	}

	//check if all vertices are group independent
	map<unsigned, string> ver_map;
	for(it = grp_map.begin(); it != grp_map.end(); ++it){

		set<unsigned> grp_vers = it->second.get_vertices();
		set<unsigned>::iterator vert_it = grp_vers.begin();

		for(; vert_it != grp_vers.end(); ++vert_it){

			if(ver_map.find(*vert_it) != ver_map.end()){
				stringstream e;
				e << "Error: Vertex number: "<<*vert_it<<" was referenced in";
				e << " both group "<<ver_map[*vert_it]<<" and group ";
				e <<it->second.get_name()<<".\n";
				throw e.str();
			}

			ver_map[*vert_it] = it->second.get_name();
		}//for
	}//for
}//check_integrity

/*
 * Throws a std::string exception for Unexpected Tokens
 * @param tok - The unexpected Token
 * @param expected - A string of Token names that you expected to recieve
*/
void Parser::unexpectedToken(Token tok, string expected){
	stringstream e;
	e<<"ERROR: Unexpected Token "<<tok.line<<":"<<tok.pos;
	e<<" Got "<<(string)tok;
	e<<" Expected {" <<expected<<"}.\n";
	throw e.str();
}

/*
 * Throws a std::string exception for any generic error
 * @param tok - Token that caused the error
 * @param error - The specific error message that is to be thrown
*/
void Parser::generic_error(Token tok, string error){
	stringstream e;
	e<<"ERROR: "<<tok.line<<":"<<tok.pos;
	e<<error<<'\n';
	throw e.str();
}

/*
 * Method for creating a new group for the parser. 
 * Modifies the Lexer to point at the next line
 * @ret - A new Group
*/
Group Parser::create_group(map<string, Group>& grp_map){
	Group newGroup;
	Token next = lex.getToken();
	while(next != Token::NEWLINE){
		if(next != Token::ID && next != Token::NEWLINE){
			unexpectedToken(next, "ID");
		}
		if(grp_map.find(next.get_data()) != grp_map.end()){
			return grp_map[next.get_data()];
		}
		newGroup.set_name(next.get_data());
		next = lex.getToken();
	}
	return newGroup;
}//create group

/*
 * Creates a new face, and modifies the lexer's position
 * @ret - A new Face
*/
Face Parser::create_face(VertexList& v_list, Material& curr_material){
	Token next = lex.getToken();
	Face f(v_list, curr_material.get_name());

	while(next != Token::NEWLINE && next != Token::END_OF_FILE){
		if(next != Token::INTEGER){
			unexpectedToken(next, "INTEGER");
		}
		int temp;
		stringstream strtoint;
		strtoint << next.get_data();
		strtoint >> temp;
		if(temp < 0){
			generic_error(next, "Reference to invalid vertex");
		}
		f.add_vertex((unsigned)temp);
		next = lex.getToken();
	}//while
	return f;
}//create face

/*
 * Addes a vertex to the Vertex List, and modifies the position of the Lexer
 * @param v_list - A VertexList that stores the vertices of the system
*/
unsigned Parser::create_vertex(VertexList &v_list){
	Token next = lex.getToken();
	stringstream conv;
	double x = 0, y = 0, z = 0, w = 1;

	//There are either 3 or four doubles
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	
	conv << next.get_data();
	conv >> x;
	conv.str("");
	conv.clear();
	
	next = lex.getToken();
	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}

	conv << next.get_data();
	conv >> y;
	conv.str("");
	conv.clear();

	next = lex.getToken();

	if(next != Token::FLOAT && next != Token::INTEGER){
		unexpectedToken(next, "FLOAT, INTEGER");
	}
	
	conv << next.get_data();
	conv >> z;
	conv.str("");
	conv.clear();

	next = lex.getToken();
	
	if(next == Token::INTEGER || next == Token::FLOAT){
		conv << next.get_data();
		conv >> w;
		conv.str("");
		conv.clear();
		next = lex.getToken();
	}

	if(next != Token::NEWLINE && next != Token::END_OF_FILE)
		unexpectedToken(next,"NEWLINE, EOF");

	return v_list.add_vertex(x, y, z, w);
}//create vertex

/*
 * Create a blank material and add it to the map
 * @param material_map - The map to add the new Material to
 * @ret - a blank material with a new 
*/
Material Parser::create_material(map<string, Material>& material_map){
	Token next = lex.getToken();
	if(next != Token::ID){
		unexpectedToken(next, "ID");
	}
	string name = next.get_data();
	material_map[name] = Material(name);
	return name;
}
