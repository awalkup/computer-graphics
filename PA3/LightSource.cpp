/*
 * Austin Walkup
 * Oct 23, 2011
 *
 * Light Source class file
 * 		Class the holds the properties for a light source
*/

#include "LightSource.h"

/*
 * Default Constructor
*/
LightSource::LightSource(): light_source(Vector()) {}

/*
 * Constructor
 * @param x - x-coordinate of the light source
 * @param y - y-coordinate of the light source
 * @param z - z-coordinate of the light source
 * @param w - homogeneous corrdinate of the light source
 * @param r - The red value of the light source's color
 * @param g - The green value of the light source's color
 * @param b - The blue value of the light source's color
*/
LightSource::LightSource(double x, double y, double z, double w,
						 double r, double g, double b): 
						 light_source(Vector(x, y, z, w)) {
	light_color.r = r;
	light_color.g = g;
	light_color.b = b;
}

