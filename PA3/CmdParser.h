/*
 * Austin Walkup
 * Sep 5, 2011
 *
 * CmdParser header file
 * 		Parser that has methods to parse a command file 
 *
*/

#ifndef __CMD_PARSER_H__WALKUP__
#define __CMD_PARSER_H__WALKUP__

#include <string>
#include <sstream>
#include <map>
#include <algorithm>
#include <vector>
#include "Token.h"
#include "CmdLexer.h"
#include "Matrix.h"
#include "Camera.h"
#include "Material.h"
#include "LightSource.h"

typedef struct CmdData {
	CmdData(std::map<std::string, Camera>& c, std::map<std::string, Material>& m, std::vector<LightSource>& l, std::string& w, std::string& s): camera_map(c), material_map(m), light_sources(l), wireFrame_camera(w), surface_render(s) {}
	std::map<std::string, Matrix> matrix_map;
	std::map<std::string, Camera>& camera_map;
	std::map<std::string, Material>& material_map;
	std::vector<LightSource>& light_sources;
	std::string& wireFrame_camera;
	std::string& surface_render;
} CmdData;


class CmdParser {
	public:
		CmdParser(CmdLexer&);
		//default destructor fine
		void parse_cmd(CmdData&);

	private:
		void parse_numbers(double*, int);
		void unexpectedToken(Token, std::string);
		void generic_error(Token, std::string);
		void create_rmatrix(std::map<std::string, Matrix>&);
		void create_smatrix(std::map<std::string, Matrix>&);
		void create_tmatrix(std::map<std::string, Matrix>&);
		void create_amatrix(std::map<std::string, Matrix>&);
		void create_camera(std::map<std::string, Camera>&);
		void create_material(std::map<std::string, Material>&);
		void create_lightSource(std::vector<LightSource>&);
		std::string create_wireFrame(std::map<std::string, Camera>&);
		std::string create_surfaceRender(std::map<std::string, Camera>&);
		CmdLexer &lex;
};

#endif /* __CMD_PARSER_H__WALKUP__ */
